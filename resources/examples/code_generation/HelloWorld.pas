program HelloWorld;

var
    i : integer;
    s : char;
    arr : array [1..10] of integer;

procedure helloWorldProc;
begin
    writeln(0);
    writeln('Hello, world!');

    i := -3;
    writeln(i);
end;

function f : integer;
begin
    f := 0;
end;

procedure print(m1, m2 : char);
var
    m3 : char;
begin
    m1 := 'hi';
    writeln(m1);
    writeln(m2);
    m3 := 'privet';
    writeln(m3);
end;

begin
    helloWorldProc;
    writeln(f());
    writeln(1 + 2);
    writeln((1 + 2) * 3);
    writeln(1 - 2);
    writeln(7 * 8);
    writeln(4 / 2);
    writeln(-4);
    writeln(1 + 2 * 3);

    i := 1;
    writeln(i);
    s := 'hi';
    writeln(s);

    s := readln();
    writeln(s);

    print('hee''hehe', 'hohoho');

    arr[1] := 5;
    writeln(arr[1]);
    arr[1] := 6;
    writeln(arr[1]);
end.
