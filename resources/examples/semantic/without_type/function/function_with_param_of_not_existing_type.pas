program Fibonacci;

function min(a, b: float) : integer;
begin
    if (a > b) then
        min := b
    else
        min := a;
end;

begin
    min(1, min(2, 3));
end.
