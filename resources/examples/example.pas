program Fibonacci;

const
    PI = 3;
    E = 2;
    NEG42 = -42;
    STR = '4 8 15 16 23 42';
    COPY = STR;
    ANOTHERCOPY = -NEG42;

var
    day : integer;
    name : char;
    a, b, c : integer;
    arr : array [1..10] of integer;

function f : integer;
var
    result : integer;
begin
    result := 0;
    f := result + E;
end;

function min(a, b: integer) : integer;
begin
    if (a > b) then
        min := b
    else
        min := a;
end;

procedure helloWorld;
const
    PI = 4;
var
    greeting : char;
begin
    greeting := 'hello world';
    writeln(greeting);
end;

begin
    name := 'Alexander';
    writeln(name);

    readln(day);

    a := -1;
    b := 2;
    c := 3;

    a := a + 1;

    c := a + b * c / a - b;
    c := (a + b) * c / a - b;
    arr[1] := 1;
    arr[10] := 10;
    a := arr[5];

    a := NEG42;

    if a = 0 then
        a := 1;

    if a = 0 then
        if b = 0 then
            a := 1;

    if b <> 4 and c > 5 or a then
    begin
        b := 1;
        c := 2;
    end;

    if a = 1 then
        writeln(a)
    else
        writeln(b);

    if a = 2 then
    begin
        writeln(a);
    end
    else
    begin
        writeln(b);
    end;

    if (a <= 2) then
        a := 1
    else if (a > 2) then
        a := a + 1
    else
        writeln('Warning!');

    case a of
        1: writeln(a);
        2: begin writeln(a); writeln(b); end;
        3: writeln(a);
    end;

    while a < 5 do
    begin
        writeln('hi');
        a := a + 1;
    end;

    repeat
    begin
        a := 3 + 2;
        writeln('done');
    end;
    until a < 3;

    for b := 20 downto 16 do
        writeln('Hi');

    for b := 20 to 16 do
        writeln('Hi');

    min(1, min(2, 3));
end.
