import java.io.PrintStream;
import java.util.Scanner;

public class RTL {
    static PrintStream out = System.out;
    static Scanner scanner = new Scanner(System.in);

    public static void printString(String str) {
        out.println(str);
    }

    public static void printInt(int value) {
        out.println(value);
    }

    public static int scanInt() {
        return scanner.nextInt();
    }

    public static String scanString() {
        return scanner.nextLine();
    }
}
