﻿#include "LexerTest.hpp"
#include "../lib/Lexer.hpp"
#include "../lib/Driver.hpp"
#include <sstream>

void LexerTest::test_data()
{
    QTest::addColumn<QString>("input");
    QTest::addColumn<Values>("expectation");

    QTest::newRow("some tokens")
         << "-12 0.5e-10 haha 'hi''hi' {com} (*another com*) while :="
         << Values{
                VALUE("", "", MINUS),
                VALUE("long long", 12, INT),
                VALUE("double", 0.5e-10, REAL),
                VALUE("string", "haha", ID),
                VALUE("string", "hi'hi", STRING),
                VALUE("", "", WHILE),
                VALUE("", "", ASSIGN),
            };

    QTest::newRow("comments")
        << "{com*)0{com}1(*com}2(*com*)12"
        << Values{
                VALUE("long long", 0, INT),
                VALUE("long long", 1, INT),
                VALUE("long long", 2, INT),
                VALUE("long long", 12, INT),
           };
}

void LexerTest::test()
{
    QFETCH(QString, input);
    QFETCH(Values, expectation);

    pascal2java::Driver d;
    std::istringstream is{input.toStdString()};
    pascal2java::Lexer l{&is};

    for (auto e : expectation)
    {
        auto real = l.lex(d);
        auto type = real.token();

        QVERIFY2(std::get<2>(e) == type,
            QString("\nExpected:\n\"%1\"\n\nReal:\n\"%2\"\n")
            .arg(std::get<2>(e)).arg(type).toStdString().c_str());

        QString message{"\nExpected:\n\"%1\"\n\nReal:\n\"%2\"\n"};

        if (std::get<0>(e) == "long long")
        {
            auto v = real.value.as<long long>();
            QVERIFY2(std::get<1>(e).toLongLong() == v,
                message.arg(std::get<1>(e).toLongLong()).arg(v)
                .toStdString().c_str());
        }
        else if (std::get<0>(e) == "double")
        {
            auto v = real.value.as<double>();
            QVERIFY2(std::get<1>(e).toDouble() == v,
                message.arg(std::get<1>(e).toDouble()).arg(v)
                .toStdString().c_str());
        }
        else if (std::get<0>(e) == "string")
        {
            auto v = real.value.as<QString>();
            QVERIFY2(std::get<1>(e).toString() == v,
                message.arg(std::get<1>(e).toString()).arg(v)
                .toStdString().c_str());
        }
    }
}
