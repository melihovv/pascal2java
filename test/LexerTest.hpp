﻿#ifndef LEXERTEST_H
#define LEXERTEST_H

#include <QObject>
#include <QTest>
#include <QVector>
#include <tuple>
#include "TestRunner.hpp"
#include "../lib/parser.tab.hh"

typedef QVector<std::tuple<QString, QVariant, int>> Values;
Q_DECLARE_METATYPE(Values);

#define VALUE(type, value, token_type) \
    std::make_tuple(type, \
                    QVariant(value), \
                    pascal2java::Parser::token::TOK_##token_type)

class LexerTest : public QObject
{
    Q_OBJECT

private slots:
    void test_data();
    void test();
};

DECLARE_TEST(LexerTest);

#endif // LEXERTEST_H
