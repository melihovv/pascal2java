﻿#ifndef DOTGENERATORVISITOR_H
#define DOTGENERATORVISITOR_H

#include "IVisitor.hpp"
#include <QtCore/QString>

namespace pascal2java
{
    class DotGeneratorVisitor : public IVisitor
    {
    public:
        virtual void visit(Program* program) override;
        virtual void visit(Block* block) override;
        virtual void visit(TypeId* type_id) override;
        virtual void visit(TypeArray* type_array) override;
        virtual void visit(SubrangeType* subrange_type) override;
        virtual void visit(ProcedureDeclaration* proc_decl) override;
        virtual void visit(ValueParameterSpecification* value_par_spec)
            override;
        virtual void visit(VariableParameterSpecification* value_par_spec)
            override;
        virtual void visit(FunctionDeclaration* func_decl) override;
        virtual void visit(FunctionHeading* func_heading) override;
        virtual void visit(FormalParameterSection* formal_param_sec)
            override;
        virtual void visit(ProcedureIdentification* proc_ident) override;
        virtual void visit(FunctionIdentification* func_ident) override;
        virtual void visit(ProcedureHeading* proc_heading) override;
        virtual void visit(ProcOrFuncDeclaration* proc_or_func_decl)
            override;
        virtual void visit(ActualParameter* actual_parameter) override;
        virtual void visit(FunctionDesignator* func_designator) override;
        virtual void visit(VariableIdAccess* var_id_access) override;
        virtual void visit(IndexedVariable* indexed_variable) override;
        virtual void visit(WriteToIndexedVariable* indexed_variable)
            override;
        virtual void visit(AssignmentStatement* statement) override;
        virtual void visit(ProcedureStatement* statement) override;
        virtual void visit(CompoundStatement* statement) override;
        virtual void visit(CaseStatement* statement) override;
        virtual void visit(CaseListElement* case_list_elem) override;
        virtual void visit(RepeatStatement* statement) override;
        virtual void visit(IfWithoutElseStatement* statement) override;
        virtual void visit(IfElseStatement* statement) override;
        virtual void visit(WhileStatement* statement) override;
        virtual void visit(ForToStatement* statement) override;
        virtual void visit(ForDowntoStatement* statement) override;
        virtual void visit(EqualExpr* equal_expr) override;
        virtual void visit(NotEqualExpr* not_equal_expr) override;
        virtual void visit(LtExpr* lt_expr) override;
        virtual void visit(GtExpr* gt_expr) override;
        virtual void visit(LeExpr* le_expr) override;
        virtual void visit(GeExpr* ge_expr) override;
        virtual void visit(AndExpr* and_expr) override;
        virtual void visit(OrExpr* or_expr) override;
        virtual void visit(NotExpr* not_expr) override;
        virtual void visit(PlusExpr* plus_expr) override;
        virtual void visit(MinusExpr* minus_expr) override;
        virtual void visit(StarExpr* star_expr) override;
        virtual void visit(SlashExpr* slash_expr) override;
        virtual void visit(DivExpr* div_expr) override;
        virtual void visit(ModExpr* mod_expr) override;
        virtual void visit(UnaryMinus* minus) override;
        virtual void visit(Constant* constant) override;

        QString dot() const
        {
            return dot_;
        }

    private:
        QString dot_;
        QString block_parent_;
        QString stmt_parent_ = "";
        int counter_a_ = 0;
        int counter_b_ = 0;
        int counter_c_ = 0;
        int counter_d_ = 0;
        int counter_e_ = 0;
        int counter_f_ = 0;
        int counter_g_ = 0;
        int counter_h_ = 0;
        int counter_i_ = 0;
        int counter_j_ = 0;
        int counter_k_ = 0;
        int counter_l_ = 0;
        int counter_m_ = 0;
        int counter_n_ = 0;
        int counter_o_ = 0;
        int counter_p_ = 0;
        int counter_q_ = 0;
        int counter_r_ = 0;
        int counter_s_ = 0;
        int counter_t_ = 0;
        int counter_u_ = 0;
        int counter_v_ = 0;
        int counter_x_ = 0;
        int counter_y_ = 0;
        int counter_z_ = 0;
        int counter_aa_ = 0;
        int counter_bb_ = 0;
        int counter_cc_ = 0;
        int counter_dd_ = 0;
        int counter_ee_ = 0;
        int counter_ff_ = 0;
        int counter_gg_ = 0;
        int counter_hh_ = 0;
        int counter_ii_ = 0;
        int counter_jj_ = 0;
        int counter_kk_ = 0;
        int counter_ll_ = 0;
        int counter_mm_ = 0;
        int counter_nn_ = 0;
        int counter_oo_ = 0;
        int counter_pp_ = 0;
        int counter_qq_ = 0;
        int counter_rr_ = 0;
        int counter_ss_ = 0;
        int counter_tt_ = 0;
        int counter_uu_ = 0;
        int counter_vv_ = 0;
        int counter_xx_ = 0;
        int counter_yy_ = 0;
        int counter_zz_ = 0;
        int counter_aaa_ = 0;
        int counter_bbb_ = 0;
        int counter_ccc_ = 0;
        int counter_ddd_ = 0;
        int counter_eee_ = 0;
        int counter_fff_ = 0;
        int counter_ggg_ = 0;
        int counter_hhh_ = 0;
        int counter_iii_ = 0;
    };
}

#endif // DOTGENERATORVISITOR_H
