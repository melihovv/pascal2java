﻿#include "SemanticErrorsCheckerWithoutTypeInfoVisitor.hpp"
#include "Ast.hpp"

const QSet<QString>
pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::types_ =
QSet<QString>{
    "integer",
    "char"
};

QSet<QString>
pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::func_ids_ =
QSet<QString>{
    "writeln",
    "readln",
};

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    Program* program)
{
    program_name_ = program->name_;
    parent_name_ = program_name_;

    global_ids_ << program->name_;
    funcs_ = program->block_->proc_and_func_;

    program->block_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    Block* block)
{
    for (const std::pair<QString, Expr*>& constant : block->constants_)
    {
        if (is_global_context() && global_ids_.contains(constant.first) ||
            ids_.contains(constant.first))
        {
            throw_exception("Duplicate id: " + constant.first);
        }
        else
        {
            if (is_global_context())
            {
                global_ids_ << constant.first;
            }
            else
            {
                ids_ << constant.first;
            }
        }

        if (is_global_context() && global_constants_.size() == 0)
        {
            global_constants_ = block->constants_;
        }
        else if (!is_global_context())
        {
            constants_ = block->constants_;
        }

        constant.second->accept(*this);
    }

    for (const auto& var : block->vars_)
    {
        if (is_global_context() && global_ids_.contains(var.first) ||
            ids_.contains(var.first))
        {
            throw_exception("Duplicate id: " + var.first);
        }
        else
        {
            if (is_global_context())
            {
                global_ids_ << var.first;
            }
            else
            {
                ids_ << var.first;
            }
        }

        var.second->accept(*this);
    }

    for (auto f : block->proc_and_func_)
    {
        f->accept(*this);
    }

    block->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    TypeId* type_id)
{
    if (!types_.contains(type_id->type_))
    {
        throw_exception("Illegal type: " + type_id->type_);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    TypeArray* type_array)
{
    type_array->type_denoter_->accept(*this);

    for (auto l : type_array->index_type_list_)
    {
        l->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    SubrangeType* subrange_type)
{
    if (subrange_type->min()->const_type() != Constant::Type::NUMBER ||
        subrange_type->min()->get_number().get_int() < 0)
    {
        throw_exception("Invalid subrange type");
    }

    if (subrange_type->max()->const_type() != Constant::Type::NUMBER ||
        subrange_type->max()->get_number().get_int() < 0)
    {
        throw_exception("Invalid subrange type");
    }

    if (subrange_type->min()->get_number().get_int() >=
        subrange_type->max()->get_number().get_int())
    {
        throw_exception("Invalid subrange type");
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    FunctionDeclaration* func_decl)
{
    QString id = func_decl->heading_->name_->name_;

    if (global_ids_.contains(id))
    {
        throw_exception("Duplicate id: " + id);
    }

    global_ids_ << id;
    ids_ << id;
    func_ids_ << id;

    for (auto p : func_decl->heading_->params_)
    {
        p->val_param_spec_->accept(*this);
    }

    func_decl->heading_->return_type_->accept(*this);

    QString temp = parent_name_;
    parent_name_ = func_decl->heading_->name_->name_;
    func_decl->body_->accept(*this);
    parent_name_ = temp;

    ids_.clear();
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ProcedureDeclaration* proc_decl)
{
    QString id = proc_decl->heading_->name_->name_;

    if (global_ids_.contains(id))
    {
        throw_exception("Duplicate id: " + id);
    }

    global_ids_ << id;
    ids_ << id;
    func_ids_ << id;

    for (auto p : proc_decl->heading_->params_)
    {
        p->val_param_spec_->accept(*this);
    }

    QString temp = parent_name_;
    parent_name_ = proc_decl->heading_->name_->name_;
    proc_decl->body_->accept(*this);
    parent_name_ = temp;

    ids_.clear();
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
    for (auto v : val_par_spec->vars_)
    {
        if (ids_.contains(v.first))
        {
            throw_exception("Duplicate id: " + v.first);
        }

        ids_ << v.first;
        v.second->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    FunctionHeading* func_heading)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ProcedureIdentification* proc_ident)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    FunctionIdentification* func_ident)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ProcedureHeading* proc_heading)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }
    else if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    FunctionDesignator* func_designator)
{
    if (!func_ids_.contains(func_designator->id_))
    {
        throw_exception(
            "Call of not existing function: " + func_designator->id_);
    }

    for (auto p : func_designator->params_)
    {
        p->accept(*this);
    }

    int params_amount = func_designator->params_.size();
    for (auto f : funcs_)
    {
        if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (f->func_decl_->heading_->name_->name_ == func_designator->id_)
            {
                if (f->func_decl_->heading_->params_amount() != params_amount)
                {
                    throw_exception(
                        "Function call with invalid argument count");
                }
            }
        }
        else if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (f->proc_decl_->heading_->name_->name_ == func_designator->id_)
            {
                if (f->proc_decl_->heading_->params_amount() != params_amount)
                {
                    throw_exception(
                        "Function call with invalid argument count");
                }
            }
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    VariableIdAccess* var_id_access)
{
    QString id = var_id_access->id_;
    if (is_global_context() && !global_ids_.contains(id) ||
        !ids_.contains(id) && !global_ids_.contains(id))
    {
        throw_exception(
            "Use of undeclared constant/variable/function/procedure: " +
            var_id_access->id_);
    }

    if (!is_global_context())
    {
        for (auto f : funcs_)
        {
            if (f->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
            {
                if (f->proc_decl_->heading_->name_->name_ == id)
                {
                    throw_exception("Cannot return value from procedure: " +
                        parent_name_);
                }
            }
        }
    }

    if (check_assignment_to_constant_)
    {
        if (is_global_context())
        {
            for (auto c : global_constants_)
            {
                if (var_id_access->id_ == c.first)
                {
                    throw_exception("Assignment to constant");
                }
            }
        }
        else
        {
            for (auto c : constants_)
            {
                if (var_id_access->id_ == c.first)
                {
                    throw_exception("Assignment to constant");
                }
            }

            for (auto c : global_constants_)
            {
                if (var_id_access->id_ == c.first)
                {
                    throw_exception("Assignment to constant");
                }
            }
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    IndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (auto e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (auto e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    AssignmentStatement* statement)
{
    check_assignment_to_constant_ = true;
    statement->var_access_->accept(*this);
    check_assignment_to_constant_ = false;
    statement->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ProcedureStatement* statement)
{
    if (!func_ids_.contains(statement->id_))
    {
        throw_exception("Call of not existing function: " + statement->id_);
    }

    int params_amount = statement->params_.size();
    for (auto f : funcs_)
    {
        if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (f->func_decl_->heading_->name_->name_ == statement->id_)
            {
                if (f->func_decl_->heading_->params_amount() != params_amount)
                {
                    throw_exception(
                        "Function call with invalid argument count");
                }
            }
        }
        else if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (f->proc_decl_->heading_->name_->name_ == statement->id_)
            {
                if (f->proc_decl_->heading_->params_amount() != params_amount)
                {
                    throw_exception(
                        "Function call with invalid argument count");
                }
            }
        }
    }

    for (ActualParameter* p : statement->params_)
    {
        p->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    CaseStatement* statement)
{
    statement->expr_->accept(*this);

    for (CaseListElement* e : statement->elems_)
    {
        e->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    CaseListElement* case_list_elem)
{
    for (Constant* c : case_list_elem->constants_)
    {
        c->accept(*this);
    }

    case_list_elem->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    RepeatStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    IfWithoutElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    IfElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
    statement->else_stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    WhileStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ForToStatement* statement)
{
    if (is_global_context() && !global_ids_.contains(statement->id_) ||
        !ids_.contains(statement->id_) && !global_ids_.contains(statement->id_))
    {
        throw_exception("Undeclared variable: " + statement->id_);
    }

    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ForDowntoStatement* statement)
{
    if (is_global_context() && !global_ids_.contains(statement->id_) ||
        !ids_.contains(statement->id_) && !global_ids_.contains(statement->id_))
    {
        throw_exception("Undeclared variable: " + statement->id_);
    }

    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    EqualExpr* equal_expr)
{
    equal_expr->left_->accept(*this);
    equal_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    NotEqualExpr* not_equal_expr)
{
    not_equal_expr->left_->accept(*this);
    not_equal_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    LtExpr* lt_expr)
{
    lt_expr->left_->accept(*this);
    lt_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    GtExpr* gt_expr)
{
    gt_expr->left_->accept(*this);
    gt_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    LeExpr* le_expr)
{
    le_expr->left_->accept(*this);
    le_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    GeExpr* ge_expr)
{
    ge_expr->left_->accept(*this);
    ge_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    AndExpr* and_expr)
{
    and_expr->left_->accept(*this);
    and_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    OrExpr* or_expr)
{
    or_expr->left_->accept(*this);
    or_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    NotExpr* not_expr)
{
    not_expr->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    PlusExpr* plus_expr)
{
    plus_expr->left_->accept(*this);
    plus_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    MinusExpr* minus_expr)
{
    minus_expr->left_->accept(*this);
    minus_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    StarExpr* star_expr)
{
    star_expr->left_->accept(*this);
    star_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    SlashExpr* slash_expr)
{
    slash_expr->left_->accept(*this);
    slash_expr->right_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    DivExpr* div_expr)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    ModExpr* mod_expr)
{
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    UnaryMinus* minus)
{
    minus->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::visit(
    Constant* constant)
{
    if (constant->const_type() == Constant::Type::ID)
    {
        QString id = constant->get_string_or_id();

        if (is_global_context() && !global_ids_.contains(id) ||
            !ids_.contains(id) && !global_ids_.contains(id))
        {
            throw_exception("Not existing constant: " + id);
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::throw_exception(
    const QString& message)
{
    throw std::exception(message.toStdString().c_str());
}

bool
pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor::is_global_context()
const
{
    return parent_name_ == program_name_;
}
