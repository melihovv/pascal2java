#ifndef DRIVER_H
#define DRIVER_H

#include <QString>
#include <QList>
#include <memory>
#include <boost/lexical_cast.hpp>
#include "SyntaxError.hpp"
#include "location.hh"
#include "parser.tab.hh"
#include "Ast.hpp"
#include "ConstantTable.hpp"

namespace pascal2java
{
    /*!
     * Driver class.
     */
    class Driver
    {
    public:
        Driver(std::istream* is = nullptr);
        ~Driver();

        /*!
         * Parses input and return result.
         *\return Result of arithmetic expression.
         */
        void parse();

        /*!
         * Switches lexer input stream. Default is standard input
         * (std::cin).
         *\param[in] is Input stream.
         */
        void switch_input_stream(std::istream* is) const;

        /*!
         * Sets name of parsing file.
         *\param[in] fileName Name of parsing file.
         */
        void set_file_name(const std::string& file_name);

        /*
         * Returns all errors.
         */
        QList<SyntaxError> errors() const;

        /*
         * Returns ast root.
         */
        Program* root() const;

        QMap<QString, ConstantTable>& constants();

        friend class Parser;
        friend class Lexer;

    private:
        /*!
         * Adds error in the list of errors.
         *\param[in] location Location of error.
         *\param[in] message Error message.
         */
        void add_error(
            const location& location,
            const QString& message
        );

        /*
         * Converts string str to type T.
         * If conversion was unsuccessful returns 0 and adds error message to
         * errors list.
         *
         *\param[in] str String for conversion.
         *\param[in] location Location of string.
         */
        template <typename T>
        T to(const char* str, const location& location)
        {
            try
            {
                return boost::lexical_cast<T>(str);
            }
            catch (const boost::bad_lexical_cast& e)
            {
                add_error(location, "Too big (small) number");
                return 0;
            }
        }

        std::string file_name_;
        QList<SyntaxError> errors_;
        std::unique_ptr<Lexer> lexer_;
        std::unique_ptr<Parser> parser_;
        Program* program_;
        QMap<QString, ConstantTable> constants_;
    };
}

#endif // DRIVER_H
