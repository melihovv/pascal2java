﻿#include "DeletingVisitor.hpp"
#include "../lib/Ast.hpp"

void pascal2java::DeletingVisitor::visit(Program* program)
{
    program->block_->accept(*this);
    delete program;
}

void pascal2java::DeletingVisitor::visit(Block* block)
{
    for (const std::pair<QString, Expr*>& e : block->constants_)
    {
        e.second->accept(*this);
    }

    for (Type* t : block->types_)
    {
        t->accept(*this);
    }

    for (const auto& v : block->vars_)
    {
        v.second->accept(*this);
    }

    for (ProcOrFuncDeclaration* e : block->proc_and_func_)
    {
        e->accept(*this);
    }

    block->stmt_->accept(*this);

    delete block;
}

void pascal2java::DeletingVisitor::visit(TypeId* type_id)
{
    delete type_id;
}

void pascal2java::DeletingVisitor::visit(TypeArray* type_array)
{
    delete type_array->type_denoter_;

    for (auto& n : type_array->index_type_list_)
    {
        n->accept(*this);
    }

    delete type_array;
}

void pascal2java::DeletingVisitor::visit(SubrangeType* subrange_type)
{
    subrange_type->min()->accept(*this);
    subrange_type->max()->accept(*this);
    delete subrange_type;
}

void pascal2java::DeletingVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->proc_decl_ != nullptr)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }

    if (proc_or_func_decl->func_decl_ != nullptr)
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }

    delete proc_or_func_decl;
}

void pascal2java::DeletingVisitor::visit(ProcedureHeading* proc_heading)
{
    proc_heading->name_->accept(*this);

    for (auto p : proc_heading->params_)
    {
        p->accept(*this);
    }

    delete proc_heading;
}

void pascal2java::DeletingVisitor::visit(
    FunctionIdentification* func_ident)
{
    delete func_ident;
}

void pascal2java::DeletingVisitor::visit(
    ProcedureIdentification* proc_ident)
{
    delete proc_ident;
}

void pascal2java::DeletingVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
    if (formal_param_sec->val_param_spec_ != nullptr)
    {
        formal_param_sec->val_param_spec_->accept(*this);
    }

    if (formal_param_sec->var_param_spec_ != nullptr)
    {
        formal_param_sec->var_param_spec_->accept(*this);
    }

    delete formal_param_sec;
}

void pascal2java::DeletingVisitor::visit(FunctionHeading* func_heading)
{
    func_heading->name_->accept(*this);
    func_heading->return_type_->accept(*this);

    for (auto p : func_heading->params_)
    {
        p->accept(*this);
    }

    delete func_heading;
}

void pascal2java::DeletingVisitor::visit(FunctionDeclaration* func_decl)
{
    func_decl->heading_->accept(*this);
    func_decl->body_->accept(*this);

    delete func_decl;
}

void pascal2java::DeletingVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
    for (auto t : var_par_spec->vars_)
    {
        t.second->accept(*this);
    }

    delete var_par_spec;
}

void pascal2java::DeletingVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
    for (auto t : val_par_spec->vars_)
    {
        t.second->accept(*this);
    }

    delete val_par_spec;
}

void pascal2java::DeletingVisitor::visit(ProcedureDeclaration* proc_decl)
{
    proc_decl->heading_->accept(*this);
    proc_decl->body_->accept(*this);

    delete proc_decl;
}

void pascal2java::DeletingVisitor::visit(
    ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
    delete actual_parameter;
}

void pascal2java::DeletingVisitor::visit(
    FunctionDesignator* func_designator)
{
    for (ActualParameter* p : func_designator->params_)
    {
        p->accept(*this);
    }

    delete func_designator;
}

void pascal2java::DeletingVisitor::visit(VariableIdAccess* var_id_access)
{
    delete var_id_access;
}

void pascal2java::DeletingVisitor::visit(
    IndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (Expr* e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }

    delete indexed_variable;
}

void pascal2java::DeletingVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (Expr* e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }

    delete indexed_variable;
}

void pascal2java::DeletingVisitor::visit(AssignmentStatement* statement)
{
    statement->var_access_->accept(*this);
    statement->expr_->accept(*this);
    delete statement;
}

void pascal2java::DeletingVisitor::visit(ProcedureStatement* statement)
{
    for (ActualParameter* elem : statement->params_)
    {
        elem->accept(*this);
    }

    delete statement;
}

void pascal2java::DeletingVisitor::visit(CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }

    delete statement;
}

void pascal2java::DeletingVisitor::visit(CaseStatement* statement)
{
    statement->expr_->accept(*this);

    for (CaseListElement* elem : statement->elems_)
    {
        elem->accept(*this);
    }

    delete statement;
}

void pascal2java::DeletingVisitor::visit(CaseListElement* case_list_elem)
{
    for (Constant* c : case_list_elem->constants_)
    {
        c->accept(*this);
    }

    case_list_elem->stmt_->accept(*this);

    delete case_list_elem;
}

void pascal2java::DeletingVisitor::visit(RepeatStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(
    IfWithoutElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(IfElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
    statement->else_stmt_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(WhileStatement* statement)
{
    statement->stmt_->accept(*this);
    statement->expr_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(ForToStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(ForDowntoStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);

    delete statement;
}

void pascal2java::DeletingVisitor::visit(EqualExpr* equal_expr)
{
    visitBinaryExpr(equal_expr);
}

void pascal2java::DeletingVisitor::visit(NotEqualExpr* not_equal_expr)
{
    visitBinaryExpr(not_equal_expr);
}

void pascal2java::DeletingVisitor::visit(LtExpr* lt_expr)
{
    visitBinaryExpr(lt_expr);
}

void pascal2java::DeletingVisitor::visit(GtExpr* gt_expr)
{
    visitBinaryExpr(gt_expr);
}

void pascal2java::DeletingVisitor::visit(LeExpr* le_expr)
{
    visitBinaryExpr(le_expr);
}

void pascal2java::DeletingVisitor::visit(GeExpr* ge_expr)
{
    visitBinaryExpr(ge_expr);
}

void pascal2java::DeletingVisitor::visit(AndExpr* and_expr)
{
    visitBinaryExpr(and_expr);
}

void pascal2java::DeletingVisitor::visit(OrExpr* or_expr)
{
    visitBinaryExpr(or_expr);
}

void pascal2java::DeletingVisitor::visit(NotExpr* not_expr)
{
    not_expr->expr_->accept(*this);
    delete not_expr;
}

void pascal2java::DeletingVisitor::visit(PlusExpr* plus_expr)
{
    visitBinaryExpr(plus_expr);
}

void pascal2java::DeletingVisitor::visit(MinusExpr* minus_expr)
{
    visitBinaryExpr(minus_expr);
}

void pascal2java::DeletingVisitor::visit(StarExpr* star_expr)
{
    visitBinaryExpr(star_expr);
}

void pascal2java::DeletingVisitor::visit(SlashExpr* slash_expr)
{
    visitBinaryExpr(slash_expr);
}

void pascal2java::DeletingVisitor::visit(DivExpr* div_expr)
{
    visitBinaryExpr(div_expr);
}

void pascal2java::DeletingVisitor::visit(ModExpr* mod_expr)
{
    visitBinaryExpr(mod_expr);
}

void pascal2java::DeletingVisitor::visitBinaryExpr(BinaryExpr* binExpr)
{
    binExpr->left_->accept(*this);
    binExpr->right_->accept(*this);

    visitNode(binExpr);
}

void pascal2java::DeletingVisitor::visitNode(Node* node)
{
    delete node;
    node = nullptr;
}

void pascal2java::DeletingVisitor::visit(UnaryMinus* minus)
{
    minus->expr_->accept(*this);
    delete minus;
}

void pascal2java::DeletingVisitor::visit(Constant* constant)
{
    delete constant;
}
