#ifndef IVISITOR_H
#define IVISITOR_H

namespace pascal2java
{
    struct SubrangeType;
    struct TypeArray;
    struct TypeId;
    struct OrdinalTypeId;
    struct Expr;
    struct UnaryMinus;
    struct Constant;
    struct Block;
    struct Program;
    struct FunctionDeclaration;
    struct ProcedureDeclaration;
    struct ParameterSpecification;
    struct ValueParameterSpecification;
    struct VariableParameterSpecification;
    struct FunctionHeading;
    struct FormalParameterSection;
    struct ProcedureIdentification;
    struct FunctionIdentification;
    struct ProcedureHeading;
    struct ProcOrFuncDeclaration;
    struct AssignmentStatement;
    struct CompoundStatement;
    struct ProcedureStatement;
    struct WhileStatement;
    struct IfWithoutElseStatement;
    struct IfElseStatement;
    struct CaseStatement;
    struct ForToStatement;
    struct ForDowntoStatement;
    struct RepeatStatement;
    struct IndexedVariable;
    struct WriteToIndexedVariable;
    struct VariableIdAccess;
    struct EqualExpr;
    struct NotEqualExpr;
    struct LtExpr;
    struct GtExpr;
    struct LeExpr;
    struct GeExpr;
    struct PlusExpr;
    struct MinusExpr;
    struct OrExpr;
    struct StarExpr;
    struct SlashExpr;
    struct DivExpr;
    struct ModExpr;
    struct AndExpr;
    struct ActualParameter;
    struct CaseListElement;
    struct FunctionDesignator;
    struct NotExpr;

    struct IVisitor
    {
        virtual ~IVisitor()
        {
        }

        virtual void visit(Program* program) = 0;
        virtual void visit(Block* block) = 0;
        virtual void visit(TypeId* type_id) = 0;
        virtual void visit(TypeArray* type_array) = 0;
        virtual void visit(SubrangeType* subrange_type) = 0;
        virtual void visit(FunctionDeclaration* func_decl) = 0;
        virtual void visit(ProcedureDeclaration* proc_decl) = 0;
        virtual void visit(ValueParameterSpecification* val_par_spec) = 0;
        virtual void visit(VariableParameterSpecification* var_par_spec)
            = 0;
        virtual void visit(FunctionHeading* func_heading) = 0;
        virtual void visit(FormalParameterSection* formal_param_sec) = 0;
        virtual void visit(ProcedureIdentification* proc_ident) = 0;
        virtual void visit(FunctionIdentification* func_ident) = 0;
        virtual void visit(ProcedureHeading* proc_heading) = 0;
        virtual void visit(ProcOrFuncDeclaration* proc_or_func_decl) = 0;
        virtual void visit(ActualParameter* actual_parameter) = 0;
        virtual void visit(FunctionDesignator* func_designator) = 0;
        virtual void visit(VariableIdAccess* var_id_access) = 0;
        virtual void visit(IndexedVariable* indexed_variable) = 0;
        virtual void visit(WriteToIndexedVariable* indexed_variable) = 0;
        virtual void visit(AssignmentStatement* statement) = 0;
        virtual void visit(ProcedureStatement* statement) = 0;
        virtual void visit(CompoundStatement* statement) = 0;
        virtual void visit(CaseStatement* statement) = 0;
        virtual void visit(CaseListElement* case_list_elem) = 0;
        virtual void visit(RepeatStatement* statement) = 0;
        virtual void visit(IfWithoutElseStatement* statement) = 0;
        virtual void visit(IfElseStatement* statement) = 0;
        virtual void visit(WhileStatement* statement) = 0;
        virtual void visit(ForToStatement* statement) = 0;
        virtual void visit(ForDowntoStatement* statement) = 0;
        virtual void visit(EqualExpr* equal_expr) = 0;
        virtual void visit(NotEqualExpr* not_equal_expr) = 0;
        virtual void visit(LtExpr* lt_expr) = 0;
        virtual void visit(GtExpr* gt_expr) = 0;
        virtual void visit(LeExpr* le_expr) = 0;
        virtual void visit(GeExpr* ge_expr) = 0;
        virtual void visit(AndExpr* and_expr) = 0;
        virtual void visit(OrExpr* or_expr) = 0;
        virtual void visit(NotExpr* not_expr) = 0;
        virtual void visit(PlusExpr* plus_expr) = 0;
        virtual void visit(MinusExpr* minus_expr) = 0;
        virtual void visit(StarExpr* star_expr) = 0;
        virtual void visit(SlashExpr* slash_expr) = 0;
        virtual void visit(DivExpr* div_expr) = 0;
        virtual void visit(ModExpr* mod_expr) = 0;
        virtual void visit(UnaryMinus* minus) = 0;
        virtual void visit(Constant* constant) = 0;
    };
}

#endif // IVISITOR_H
