QRegExp("")/*! Scan only one file. */
%option noyywrap

/*! Don't optimize lexer for console input */
%option never-interactive

/*!
 * Causes the default rule (that unmatched scanner input is echoed to stdout)
 * to be suppressed.
 */
%option nodefault

/*! Generate c++ class. */
%option c++
/*! Class name. */
%option yyclass="Lexer"

/* Generate debug output. Disable this for release version. */
%option debug

/* Case insensitive */
%option caseless

%{
    #include "Lexer.hpp"
    #include <QRegExp>

    /*!
     * Each time yylex is invoked, the begin position is moved onto the end
     * position.
     */
    #define YY_USER_ACTION loc.columns(yyleng);

    /*!
     * In our case yyterminate() must return Parser::symbol_type instead of
     * 0.
     */
    #define yyterminate() {return pascal2java::Parser::make_END(loc);}

    /*! Don't include unistd.h, which is not availible under MSVC++. */
    #define YY_NO_UNISTD_H

    #define TOKEN(NAME) Parser::make_##NAME(loc)
    #define TOKEN_V(NAME, VALUE) Parser::make_##NAME(VALUE, loc)
%}

id [a-zA-Z][a-zA-Z0-9]*
string '([^']|[']['])*'
blank [ \t\r\f\v]

digit [0-9]
digits {digit}+
sign [+-]

exp [e]{sign}?{digits}
int {digits}
real ({digits}[.]{digits}{exp}?|{digits}{exp})

%x IN_COMMENT

%%

%{
    /*! Tokens' location. */
    static pascal2java::Parser::location_type loc(&driver.file_name_);
    loc.step();
%}

"and"       return TOKEN(AND);
"array"     return TOKEN(ARRAY);
"case"      return TOKEN(CASE);
"const"     return TOKEN(CONST);
"div"       return TOKEN(DIV);
"do"        return TOKEN(DO);
"downto"    return TOKEN(DOWNTO);
"else"      return TOKEN(ELSE);
"end"       return TOKEN(END);
"for"       return TOKEN(FOR);
"forward"   return TOKEN(FORWARD);
"function"  return TOKEN(FUNCTION);
"goto"      return TOKEN(GOTO);
"if"        return TOKEN(IF);
"in"        return TOKEN(IN);
"label"     return TOKEN(LABEL);
"mod"       return TOKEN(MOD);
"nil"       return TOKEN(NIL);
"not"       return TOKEN(NOT);
"of"        return TOKEN(OF);
"or"        return TOKEN(OR);
"packed"    return TOKEN(PACKED);
"begin"     return TOKEN(BEGIN);
"file"      return TOKEN(FILE);
"procedure" return TOKEN(PROCEDURE);
"program"   return TOKEN(PROGRAM);
"record"    return TOKEN(RECORD);
"repeat"    return TOKEN(REPEAT);
"set"       return TOKEN(SET);
"then"      return TOKEN(THEN);
"to"        return TOKEN(TO);
"type"      return TOKEN(TYPE);
"until"     return TOKEN(UNTIL);
"var"       return TOKEN(VAR);
"while"     return TOKEN(WHILE);
"with"      return TOKEN(WITH);

":="        return TOKEN(ASSIGN);
":"         return TOKEN(COLON);
";"         return TOKEN(SEMICOLON);
","         return TOKEN(COMMA);
"."         return TOKEN(DOT);
".."        return TOKEN(DOUBLE_DOT);
"^"         return TOKEN(UP_ARROW);
"@"         return TOKEN(UP_ARROW);
"["         return TOKEN(LBRACKET);
"(."        return TOKEN(LBRACKET);
"]"         return TOKEN(RBRACKET);
".)"        return TOKEN(RBRACKET);
"("         return TOKEN(LPAREN);
")"         return TOKEN(RPAREN);

"="         return TOKEN(EQUAL);
"<>"        return TOKEN(NOT_EQUAL);
">"         return TOKEN(GT);
"<"         return TOKEN(LT);
">="        return TOKEN(GE);
"<="        return TOKEN(LE);

"+"         return TOKEN(PLUS);
"-"         return TOKEN(MINUS);
"*"         return TOKEN(STAR);
"/"         return TOKEN(SLASH);

{id}        return TOKEN_V(ID, yytext);
{string}    {
                return TOKEN_V(
                    STRING,
                    QString(yytext)
                        .remove(QRegExp("(^'|'$)"))
                        .replace(QRegExp("''"), "'")
                );
            }

{int}       return TOKEN_V(INT, driver.to<long long>(yytext, loc));
{real}      return TOKEN_V(REAL, driver.to<double>(yytext, loc));

{blank}+    loc.step();
<*>\n       {loc.lines(yyleng); loc.step();}

"{"         BEGIN(IN_COMMENT);
"(*"        BEGIN(IN_COMMENT);
<IN_COMMENT>{
    "}"     BEGIN(INITIAL);
    "*)"    BEGIN(INITIAL);
    [^}*\n]+
    "*"
}

. {
    driver.add_error(loc, QString("Strange symbol: ") + yytext);
}

<<EOF>> {
    loc.initialize(&driver.file_name_);
    return TOKEN(EOF);
}
%%

