/*! Require bison >=3.0.4. */
%require "3.0.4"

/*! Use c++ skeleton file. */
%skeleton "lalr1.cc"

/*! Generate header file. */
%defines

/*! For parser trace. Disable this for release version. */
%debug

/*! Verbose error messages. */
%error-verbose

/*! Enable location tracking. */
%locations
%initial-action
{
    @$.begin.filename = @$.end.filename = &driver.file_name_;
};

%define api.namespace {pascal2java}
%define parser_class_name {Parser}
%parse-param {pascal2java::Lexer& lexer}
%parse-param {pascal2java::Driver& driver}
%lex-param {pascal2java::Driver& driver}

%code requires {
    #include <QString>
    #include <string>
    #include <tuple>
    #include "Driver.hpp"
    #include "SyntaxError.hpp"
    #include "Number.hpp"
    #include "Ast.hpp"

    namespace pascal2java {
        class Lexer;
        class SyntaxError;
        class Driver;
    }
}

%code {
    #include "Lexer.hpp"

    #define yylex(x) lexer.lex(x)

    void pascal2java::Parser::error(
            const location_type& location,
            const std::string& message)
    {
        driver.add_error(location, QString::fromStdString(message));
    }

    #define NOT_REALIZED(l) \
        driver.add_error(l, "Not realized yet"); \
        return -1;
}

%{
// This section is needed at least for proper recognation of the bison file by
// the vim. But you can also include here usefull headers or write any
// declarations.
%}

/*! Generate constructor functions for tokens. */
%define api.token.constructor

/*! Use variants instead of union. */
%define api.value.type variant

/*! Issue runtime assertions to catch invalid uses. */
%define parse.assert

%define api.token.prefix {TOK_}

%token
    EOF 0 "end of file"

    AND "and"
    ARRAY "array"
    CASE "case"
    CONST "const"
    DIV "div"
    DO "do"
    DOWNTO "downto"
    ELSE "else"
    END "end"
    FOR "for"
    FORWARD "forward"
    FUNCTION "function"
    GOTO "goto"
    IF "if"
    IN "in"
    LABEL "label"
    MOD "mod"
    NIL "nil"
    NOT "not"
    OF "of"
    OR "or"
    PACKED "packed"
    BEGIN "begin"
    FILE "file"
    PROCEDURE "procedure"
    PROGRAM "program"
    RECORD "record"
    REPEAT "repeat"
    SET "set"
    THEN "then"
    TO "to"
    TYPE "type"
    UNTIL "until"
    VAR "var"
    WHILE "while"
    WITH "with"

    ASSIGN "assignment"
    COLON "colon"
    SEMICOLON "semicolon"
    COMMA "comma"
    DOT "dot"
    DOUBLE_DOT "double dot"
    UP_ARROW "up arrow"
    LBRACKET "left bracket"
    RBRACKET "right bracket"
    LPAREN "left parenthesis"
    RPAREN "right parenthesis"

    EQUAL "equal"
    NOT_EQUAL "not equal"
    GT "greater than"
    LT "less than"
    GE "greater or equal"
    LE "less or equal"

    PLUS "plus"
    MINUS "minus"
    STAR "star"
    SLASH "slash"

    <QString> ID "id"
    <QString> STRING "string"

    <long long> INT "integer"
    <double> REAL "real"
;

%left EQUAL NOT_EQUAL LE GE LT GT IN
%left PLUS MINUS OR
%left STAR SLASH DIV MOD AND
%precedence NOT

%type<Program*> program;
%type<std::tuple<QString, QStringList>> program_heading;

%type<QStringList> id_list;
%type<Block*> block;

%type<QList<long long>> label_list;
%type<QList<long long>> label_declaration_part;

%type<QList<std::pair<QString, Expr*>>> constant_definition_part;
%type<QList<std::pair<QString, Expr*>>> constant_definition_list;
%type<std::pair<QString, Expr*>> constant_definition;
%type<Expr*> constant;

%type<QMap<QString, Type*>> type_definition_part;
%type<QMap<QString, Type*>> type_definition_list;
%type<std::pair<QString, Type*>> type_definition;
%type<Type*> type_denoter;
%type<TypeArray*> new_type;
%type<TypeArray*> new_structured_type;
%type<TypeArray*> structured_type;
%type<TypeArray*> array_type;
%type<QList<OrdinalType*>> index_type_list;
%type<OrdinalType*> ordinal_type;
%type<OrdinalType*> new_ordinal_type;
%type<OrdinalType*> subrange_type;

%type<QList<std::pair<QString, Type*>>> variable_declaration_part;
%type<QList<std::pair<QString, Type*>>> variable_declaration_list;
%type<QList<std::pair<QString, Type*>>> variable_declaration;

%type<FunctionDeclaration*> function_declaration;
%type<ProcedureDeclaration*> procedure_declaration;
%type<ProcOrFuncDeclaration*> proc_or_func_declaration;
%type<QList<ProcOrFuncDeclaration*>> proc_or_func_declaration_list;
%type<QList<ProcOrFuncDeclaration*>> procedure_and_function_declaration_part
%type<FunctionHeading*> function_heading;
%type<ProcedureHeading*> procedure_heading;
%type<FunctionIdentification*> function_identification;
%type<ProcedureIdentification*> procedure_identification;
%type<ValueParameterSpecification*> value_parameter_specification;
%type<FormalParameterSection*> formal_parameter_section;
%type<QList<FormalParameterSection*>> formal_parameter_list;
%type<QList<FormalParameterSection*>> formal_parameter_section_list;

%type<CompoundStatement*> statement_part;
%type<CompoundStatement*> compound_statement;
%type<CompoundStatement*> statement_sequence;
%type<Statement*> statement;
%type<Statement*> non_labeled_statement;
%type<AssignmentStatement*> assignment_statement;
%type<ProcedureStatement*> procedure_statement;
%type<CaseStatement*> case_statement;
%type<QList<CaseListElement*>> case_list_element_list;
%type<CaseListElement*> case_list_element;
%type<QList<Constant*>> case_constant_list;
%type<RepeatStatement*> repeat_statement;
%type<IfStatement*> if_statement;
%type<WhileStatement*> while_statement;
%type<ForStatement*> for_statement;
%type<Expr*> expression;

%type<VariableAccess*> variable_access;
%type<IndexedVariable*> indexed_variable;
%type<QList<Expr*>> index_expression_list;
%type<QList<ActualParameter*>> params;
%type<QList<ActualParameter*>> actual_parameter_list;
%type<ActualParameter*> actual_parameter;
%type<Constant*> unsigned_constant;
%type<Sign> sign;
%type<Number> unsigned_number;
%type<FunctionDesignator*> function_designator;

%start program

%%
program
    : program_heading SEMICOLON block DOT
        {driver.program_ = new Program(std::get<0>($1), std::get<1>($1), $3);}
    ;

program_heading
    : PROGRAM ID {$$ = std::make_tuple($2, QStringList());}
    | PROGRAM ID LPAREN id_list RPAREN {NOT_REALIZED(@1);}
    ;

id_list
    : ID {$$ << $1;}
    | id_list COMMA ID {$1 << $3; $$ = $1;}
    ;

block
    : label_declaration_part
      constant_definition_part
      type_definition_part
      variable_declaration_part
      procedure_and_function_declaration_part
      statement_part
        {
            if ($1.length() != 0)
            {
                NOT_REALIZED(@1);
            }

            if ($3.size() != 0)
            {
                NOT_REALIZED(@2);
            }

            $$ = new Block{QList<long long>(), $2, $3, $4, $5, $6};
        }
    ;

label_declaration_part
    : LABEL label_list SEMICOLON {$$ = $2;}
    | {}
    ;

label_list
    : INT {$$ << $1;}
    | label_list COMMA INT {$1 << $3; $$ = $1;}
    ;

constant_definition_part
    : CONST constant_definition_list {$$ = $2;}
    | {}
    ;

constant_definition_list
    : constant_definition {$$ << $1;}
    | constant_definition_list constant_definition
        {$1 << $2; $$ = $1;}
    ;

constant_definition
    : ID EQUAL constant SEMICOLON {$$ = std::make_pair($1, $3);}
    ;

constant
    : sign unsigned_number
        {
            if ($1.isMinus())
            {
                $$ = new UnaryMinus(new Constant($2));
            }
            else if ($1.isPlus())
            {
                $$ = new Constant($2);
            }
        }
    | unsigned_number {$$ = new Constant($1);}
    | sign ID
        {
            if ($1.isMinus())
            {
                $$ = new UnaryMinus(new Constant($2, Constant::Type::ID));
            }
            else if ($1.isPlus())
            {
                $$ = new Constant($2, Constant::Type::ID);
            }
        }
    | ID {$$ = new Constant($1, Constant::Type::ID);}
    | STRING {$$ = new Constant($1, Constant::Type::STRING);}
    ;

sign
    : PLUS {$$ = Sign(Sign::Type::PLUS);}
    | MINUS {$$ = Sign(Sign::Type::MINUS);}
    ;

type_definition_part
    : TYPE type_definition_list {$$ = $2;}
    | {}
    ;

type_definition_list
    : type_definition {$$.insert($1.first, $1.second);}
    | type_definition_list type_definition
        {$1.insert($2.first, $2.second); $$ = $1;}
    ;

type_definition
    : ID EQUAL type_denoter SEMICOLON {$$ = std::make_pair($1, $3);}
    ;

type_denoter
    : ID {$$ = new TypeId($1);}
    | new_type {$$ = $1;}
    ;

new_type
    : new_ordinal_type {NOT_REALIZED(@1);}
    | new_structured_type {$$ = $1;}
    | new_pointer_type {NOT_REALIZED(@1);}
    ;

new_ordinal_type
    : enumerated_type {NOT_REALIZED(@1);}
    | subrange_type {$$ = $1;}
    ;

enumerated_type
    : LPAREN id_list RPAREN
    ;

subrange_type
    : constant DOUBLE_DOT constant
        {
            $$ = new SubrangeType(
                dynamic_cast<Constant*>($1),
                dynamic_cast<Constant*>($3)
            );
        }
    ;

new_structured_type
    : PACKED structured_type {NOT_REALIZED(@1);}
    | structured_type {$$ = $1;}
    ;

structured_type
    : array_type {$$ = $1;}
    | record_type {NOT_REALIZED(@1);}
    | set_type {NOT_REALIZED(@1);}
    | file_type {NOT_REALIZED(@1);}
    ;

array_type
    : ARRAY LBRACKET index_type_list RBRACKET OF type_denoter
        {$$ = new TypeArray($6, $3);}
    ;

index_type_list
    : ordinal_type {$$ << $1;}
    | index_type_list COMMA ordinal_type {$1 << $3; $$ = $1;}
    ;

ordinal_type
    : new_ordinal_type {$$ = $1;}
    | ID {NOT_REALIZED(@1);}
    ;

record_type
    : RECORD field_list END
    ;

field_list
    : record_section_list
    | record_section_list SEMICOLON variant_part
    | variant_part
    |
    ;

record_section_list
    : record_section
    | record_section_list SEMICOLON record_section
    ;

record_section
    : id_list COLON type_denoter
    ;

variant_part
    : CASE variant_selector OF variant_list
    ;

variant_selector
    : ID COLON ID
    | ID
    ;

variant_list
    : variant
    | variant_list SEMICOLON variant
    ;

variant
    : case_constant_list COLON LPAREN field_list RPAREN
    ;

case_constant_list
    : constant {$$ << dynamic_cast<Constant*>($1);}
    | case_constant_list COMMA constant
        {$1 << dynamic_cast<Constant*>($3); $$ = $1;}
    ;

set_type
    : SET OF ordinal_type
    ;

file_type
    : FILE OF type_denoter
    ;

new_pointer_type
    : UP_ARROW ID
    ;

variable_declaration_part
    : VAR variable_declaration_list SEMICOLON {$$ = $2;}
    | {}
    ;

variable_declaration_list
    : variable_declaration {$$ = $1;}
    | variable_declaration_list SEMICOLON variable_declaration
        {
            $1.append($3);
            $$ = $1;
        }
    ;

variable_declaration
    : id_list COLON type_denoter
        {
            for (const QString& s : $1)
            {
                $$ << std::pair<QString, Type*>(s, $3->clone());
            }

            delete $3;
        }
    ;

procedure_and_function_declaration_part
    : proc_or_func_declaration_list SEMICOLON {$$ = $1;}
    | {}
    ;

proc_or_func_declaration_list
    : proc_or_func_declaration {$$ << $1;}
    | proc_or_func_declaration_list SEMICOLON proc_or_func_declaration
        {$1 << $3; $$ = $1;}
    ;

proc_or_func_declaration
    : procedure_declaration
        {
            $$ = new ProcOrFuncDeclaration(
                ProcOrFuncDeclaration::Type::PROCEDURE,
                $1,
                nullptr
            );
        }
    | function_declaration
        {
            $$ = new ProcOrFuncDeclaration(
                ProcOrFuncDeclaration::Type::FUNCTION,
                nullptr,
                $1
            );
        }
    ;

procedure_declaration
    : procedure_heading SEMICOLON FORWARD {NOT_REALIZED(@1);}
    | procedure_heading SEMICOLON block
        {
            $$ = new ProcedureDeclaration($1, $3, false);
        }
    ;

procedure_heading
    : procedure_identification {$$ = new ProcedureHeading($1);}
    | procedure_identification formal_parameter_list
        {$$ = new ProcedureHeading($1, $2);}
    ;

procedure_identification
    : PROCEDURE ID {$$ = new ProcedureIdentification($2);}
    ;

formal_parameter_list
    : LPAREN formal_parameter_section_list RPAREN {$$ = $2;}
    ;

formal_parameter_section_list
    : formal_parameter_section {$$ << $1;}
    | formal_parameter_section_list SEMICOLON formal_parameter_section
        {$1 << $3; $$ = $1;}
    ;

formal_parameter_section
    : value_parameter_specification
        {
            $$ = new FormalParameterSection(
                FormalParameterSection::Type::VALUE,
                $1,
                nullptr
            );
        }
    | variable_parameter_specification {NOT_REALIZED(@1);}
    | procedural_parameter_specification {NOT_REALIZED(@1);}
    | functional_parameter_specification {NOT_REALIZED(@1);}
    ;

value_parameter_specification
    : id_list COLON ID
        {
            QList<std::pair<QString, Type*>> tmp_values;

            for (const QString& s : $1)
            {
                tmp_values << std::make_pair(s, new TypeId($3));
            }

            $$ = new ValueParameterSpecification(tmp_values);
        }
    ;

variable_parameter_specification
    : VAR id_list COLON ID
    ;

procedural_parameter_specification
    : procedure_heading
    ;

functional_parameter_specification
    : function_heading
    ;

function_declaration
    : function_heading SEMICOLON FORWARD {NOT_REALIZED(@1);}
    | function_heading SEMICOLON block
        {
            $$ = new FunctionDeclaration($1, $3, false);
        }
    ;

function_heading
    : function_identification COLON ID
        {
            $$ = new FunctionHeading($1, new TypeId($3));
        }
    | function_identification formal_parameter_list COLON ID
        {$$ = new FunctionHeading($1, new TypeId($4), $2);}
    ;

function_identification
    : FUNCTION ID {$$ = new FunctionIdentification($2);}
    ;

statement_part
    : compound_statement {$$ = $1;}
    ;

compound_statement
    : BEGIN statement_sequence END {$$ = $2;}
    ;

statement_sequence
    : statement SEMICOLON {$$ = new CompoundStatement(); $$->append($1);}
    | statement_sequence statement SEMICOLON {$1->append($2); $$ = $1;}
    ;

statement
    : INT COLON non_labeled_statement {NOT_REALIZED(@1);}
    | non_labeled_statement {$$ = $1;}
    ;

non_labeled_statement
    : assignment_statement {$$ = $1;}
    | procedure_statement {$$ = $1;}
    | goto_statement {NOT_REALIZED(@1);}
    | compound_statement {$$ = $1;}
    | case_statement {$$ = $1;}
    | repeat_statement {$$ = $1;}
    | with_statement {NOT_REALIZED(@1);}
    | if_statement {$$ = $1;}
    | while_statement {$$ = $1;}
    | for_statement {$$ = $1;}
    ;

repeat_statement
    : REPEAT statement_sequence UNTIL expression
        {$$ = new RepeatStatement($2, $4);}
    ;

while_statement
    : WHILE expression DO statement
        {$$ = new WhileStatement($4, $2);}
    ;

for_statement
    : FOR ID ASSIGN expression TO expression DO statement
        {$$ = new ForToStatement($2, $4, $6, $8);}
    | FOR ID ASSIGN expression DOWNTO expression DO statement
        {$$ = new ForDowntoStatement($2, $4, $6, $8);}
    ;

with_statement
    : WITH record_variable_list DO statement
    ;

if_statement
    : IF expression THEN statement ELSE statement
        {$$ = new IfElseStatement($2, $4, $6);}
    | IF expression THEN statement
        {$$ = new IfWithoutElseStatement($2, $4);}
    ;

assignment_statement
    : variable_access ASSIGN expression
        {$$ = new AssignmentStatement($1, $3);}
    ;

variable_access
    : ID {$$ = new VariableIdAccess($1);}
    | indexed_variable {$$ = $1;}
    | field_designator {NOT_REALIZED(@1);}
    | variable_access UP_ARROW {NOT_REALIZED(@1);}
    ;

indexed_variable
    : variable_access LBRACKET index_expression_list RBRACKET
        {$$ = new IndexedVariable($1, $3);}
    ;

index_expression_list
    : expression {$$ << $1;}
    | index_expression_list COMMA expression {$1 << $3; $$ = $1;}
    ;

field_designator
    : variable_access DOT ID
    ;

procedure_statement
    : ID params {$$ = new ProcedureStatement($1, $2);}
    | ID {$$ = new ProcedureStatement($1, QList<ActualParameter*>());}
    ;

params
    : LPAREN actual_parameter_list RPAREN {$$ = $2;}
    ;

actual_parameter_list
    : actual_parameter {$$ << $1;}
    | actual_parameter_list COMMA actual_parameter {$1 << $3; $$ = $1;}
    | {}
    ;

actual_parameter
    : expression {$$ = new ActualParameter($1);}
    ;

goto_statement
    : GOTO INT
    ;

case_statement
    : CASE expression OF case_list_element_list END
        {$$ = new CaseStatement($2, $4);}
    | CASE expression OF case_list_element_list SEMICOLON END
        {$$ = new CaseStatement($2, $4);}
    ;

case_list_element_list
    : case_list_element {$$ << $1;}
    | case_list_element_list SEMICOLON case_list_element {$1 << $3; $$ = $1;}
    ;

case_list_element
    : case_constant_list COLON statement {$$ = new CaseListElement($1, $3);}
    ;

record_variable_list
    : variable_access
    | record_variable_list COMMA variable_access
    ;

expression
    : expression EQUAL expression {$$ = new EqualExpr($1, $3);}
    | expression NOT_EQUAL expression {$$ = new NotEqualExpr($1, $3);}
    | expression LT expression {$$ = new LtExpr($1, $3);}
    | expression GT expression {$$ = new GtExpr($1, $3);}
    | expression LE expression {$$ = new LeExpr($1, $3);}
    | expression GE expression {$$ = new GeExpr($1, $3);}
    | expression OR expression {$$ = new OrExpr($1, $3);}
    | expression AND expression {$$ = new AndExpr($1, $3);}
    | expression IN expression {NOT_REALIZED(@1);}
    | expression PLUS expression {$$ = new PlusExpr($1, $3);}
    | expression MINUS expression {$$ = new MinusExpr($1, $3);}
    | expression STAR expression {$$ = new StarExpr($1, $3);}
    | expression SLASH expression {$$ = new SlashExpr($1, $3);}
    | expression DIV expression {NOT_REALIZED(@1);}
    | expression MOD expression {NOT_REALIZED(@1);}
    | sign expression %prec NOT
        {
            if ($1.isMinus())
            {
                $$ = new UnaryMinus($2);
            }
            else if ($1.isPlus())
            {
                $$ = $2;
            }
        }
    | variable_access {$$ = $1;}
    | unsigned_constant {$$ = $1;}
    | function_designator {$$ = $1;}
    | set_constructor {NOT_REALIZED(@1);}
    | LPAREN expression RPAREN {$$ = $2;}
    | NOT expression %prec NOT {$$ = new NotExpr($2);}
    ;

unsigned_constant
    : unsigned_number {$$ = new Constant($1);}
    | STRING {$$ = new Constant($1, Constant::Type::STRING);}
    | NIL {$$ = new Constant();}
    ;

unsigned_number
    : INT {$$ = Number($1);}
    | REAL {NOT_REALIZED(@1);}
    ;

function_designator
    : ID params {$$ = new FunctionDesignator($1, $2);}
    ;

set_constructor
    : LBRACKET member_designator_list RBRACKET
    | LBRACKET RBRACKET
    ;

member_designator_list
    : member_designator
    | member_designator_list COMMA member_designator
    ;

member_designator
    : expression
    | member_designator DOUBLE_DOT expression
    ;
%%
