﻿#include "TreeAttributingVisitor.hpp"
#include <QtCore/QList>

void pascal2java::TreeAttributingVisitor::visit(Program* program)
{
    parent_name_ = program->name_;
    program_name_ = program->name_;
    program->block_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(Block* block)
{
    QList<std::pair<QString, Expr*>> temp_constants = constants_;
    QList<std::pair<QString, Type*>> temp_vars = vars_;
    QList<ProcOrFuncDeclaration*> temp_proc_and_funcs = proc_and_funcs_;

    constants_ = block->constants_;
    vars_ = block->vars_;
    proc_and_funcs_ = block->proc_and_func_;

    if (parent_name_ == program_name_)
    {
        global_constants_ = constants_;
        global_vars_ = vars_;
        global_proc_and_funcs_ = proc_and_funcs_;
    }

    if (block->constants_.size() > 0)
    {
        for (const std::pair<QString, Expr*>& constant : block->constants_)
        {
            constant.second->accept(*this);
        }
    }

    if (block->vars_.size() > 0)
    {
        for (const auto& key : block->vars_)
        {
            key.second->accept(*this);
        }
    }

    if (block->proc_and_func_.size() > 0)
    {
        for (auto f : block->proc_and_func_)
        {
            f->accept(*this);
        }
    }

    block->stmt_->accept(*this);

    constants_ = temp_constants;
    vars_ = temp_vars;
    proc_and_funcs_ = temp_proc_and_funcs;
}

void pascal2java::TreeAttributingVisitor::visit(TypeId* type_id)
{
}

void pascal2java::TreeAttributingVisitor::visit(TypeArray* type_array)
{
    for (auto l : type_array->index_type_list_)
    {
        l->accept(*this);
    }

    type_array->type_denoter_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(SubrangeType* subrange_type)
{
    subrange_type->min()->accept(*this);
    subrange_type->max()->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(FunctionDeclaration* func_decl)
{
    QString temp = parent_name_;
    parent_name_ = func_decl->heading_->name_->name_;
    func_decl->body_->accept(*this);
    parent_name_ = temp;
}

void pascal2java::TreeAttributingVisitor::visit(ProcedureDeclaration* proc_decl)
{
    QString temp = parent_name_;
    parent_name_ = proc_decl->heading_->name_->name_;
    proc_decl->body_->accept(*this);
    parent_name_ = temp;
}

void pascal2java::TreeAttributingVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::TreeAttributingVisitor::visit(FunctionHeading* func_heading)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    ProcedureIdentification* proc_ident)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    FunctionIdentification* func_ident)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    ProcedureHeading* proc_heading)
{
}

void pascal2java::TreeAttributingVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }
    else if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(
    ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(
    FunctionDesignator* func_designator)
{
    for (auto p : func_designator->params_)
    {
        p->accept(*this);
    }

    for (auto p : proc_and_funcs_)
    {
        if (p->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (p->func_decl_->heading_->name_->name_ == func_designator->id_)
            {
                auto ret_type = p->func_decl_->heading_->return_type_;
                func_designator->type_ = Expr::Type::NOT_INIT;

                if (TypeId* ti = dynamic_cast<TypeId*>(ret_type))
                {
                    if (ti->type_ == "integer")
                    {
                        func_designator->type_ = Expr::Type::INT;
                    }
                    else if (ti->type_ == "char")
                    {
                        func_designator->type_ = Expr::Type::STRING;
                    }
                }
                else if (TypeArray* ta = dynamic_cast<TypeArray*>(ret_type))
                {
                    if (TypeId* ti = dynamic_cast<TypeId*>(ta->type_denoter_))
                    {
                        if (ti->type_ == "integer")
                        {
                            func_designator->type_ = Expr::Type::INT;
                        }
                        else if (ti->type_ == "char")
                        {
                            func_designator->type_ = Expr::Type::STRING;
                        }
                    }
                }

                return;
            }
        }
        else if (p->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
        {
            if (p->proc_decl_->heading_->name_->name_ == func_designator->id_)
            {
                func_designator->type_ = Expr::Type::VOID;
                return;
            }
        }
    }
}

void pascal2java::TreeAttributingVisitor::visit(VariableIdAccess* var_id_access)
{
    for (const auto& v : vars_)
    {
        if (v.first == var_id_access->id_)
        {
            if (TypeId* ti = dynamic_cast<TypeId*>(v.second))
            {
                if (ti->type_ == "integer")
                {
                    var_id_access->type_ = Expr::Type::INT;
                    return;
                }
                else if (ti->type_ == "char")
                {
                    var_id_access->type_ = Expr::Type::STRING;
                    return;
                }
            }
            else if (TypeArray* ta = dynamic_cast<TypeArray*>(v.second))
            {
                if (TypeId* ti =
                    dynamic_cast<TypeId*>(ta->type_denoter_))
                {
                    if (ti->type_ == "integer")
                    {
                        var_id_access->type_ = Expr::Type::INT;
                    }
                    else if (ti->type_ == "char")
                    {
                        var_id_access->type_ = Expr::Type::STRING;
                    }
                }
            }
        }
    }

    for (const auto& pair : constants_)
    {
        if (pair.first == var_id_access->id_)
        {
            if (Constant* c = dynamic_cast<Constant*>(pair.second))
            {
                c->accept(*this);
                var_id_access->type_ = c->type_;
            }
            else if (UnaryMinus* u = dynamic_cast<UnaryMinus*>(pair.second))
            {
                u->accept(*this);
                var_id_access->type_ = u->type_;
            }
        }
    }

    if (var_id_access->type_ == Expr::Type::NOT_INIT)
    {
        for (const auto* p : proc_and_funcs_)
        {
            if (p->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
            {
                if (p->func_decl_->heading_->name_->name_ == var_id_access->id_)
                {
                    auto ret_type = p->func_decl_->heading_->return_type_;

                    if (TypeId* ti = dynamic_cast<TypeId*>(ret_type))
                    {
                        if (ti->type_ == "integer")
                        {
                            var_id_access->type_ = Expr::Type::INT;
                        }
                        else if (ti->type_ == "char")
                        {
                            var_id_access->type_ = Expr::Type::STRING;
                        }
                    }
                    else if (TypeArray* ta = dynamic_cast<TypeArray*>(ret_type))
                    {
                        if (TypeId* ti =
                            dynamic_cast<TypeId*>(ta->type_denoter_))
                        {
                            if (ti->type_ == "integer")
                            {
                                var_id_access->type_ = Expr::Type::INT;
                            }
                            else if (ti->type_ == "char")
                            {
                                var_id_access->type_ = Expr::Type::STRING;
                            }
                        }
                    }

                    return;
                }
            }
            else if (p->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
            {
                if (p->proc_decl_->heading_->name_->name_ == var_id_access->id_)
                {
                    var_id_access->type_ = Expr::Type::VOID;
                    return;
                }
            }
        }
    }

    if (program_name_ == parent_name_)
    {
        return;
    }

    for (const auto& v : global_vars_)
    {
        if (v.first == var_id_access->id_)
        {
            if (TypeId* ti = dynamic_cast<TypeId*>(v.second))
            {
                if (ti->type_ == "integer")
                {
                    var_id_access->type_ = Expr::Type::INT;
                    return;
                }
                else if (ti->type_ == "char")
                {
                    var_id_access->type_ = Expr::Type::STRING;
                    return;
                }
            }
        }
    }

    for (const auto& pair : global_constants_)
    {
        if (pair.first == var_id_access->id_)
        {
            if (Constant* c = dynamic_cast<Constant*>(pair.second))
            {
                if (c->const_type() == Constant::Type::NUMBER)
                {
                    var_id_access->type_ = Expr::Type::INT;
                    return;
                }
                else if (c->const_type() == Constant::Type::STRING)
                {
                    var_id_access->type_ = Expr::Type::STRING;
                    return;
                }
                else if (c->const_type() == Constant::Type::ID)
                {
                    var_id_access->type_ = Expr::Type::NOT_INIT;

                    for (const auto& constant : constants_)
                    {
                        if (constant.first == c->get_string_or_id())
                        {
                            var_id_access->type_ = constant.second->type_;
                            return;
                        }
                    }

                    return;
                }
            }
        }
    }

    if (var_id_access->type_ == Expr::Type::NOT_INIT)
    {
        for (const auto& p : global_proc_and_funcs_)
        {
            if (p->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
            {
                if (p->func_decl_->heading_->name_->name_ == var_id_access->id_)
                {
                    auto ret_type = p->func_decl_->heading_->return_type_;

                    if (TypeId* ti = dynamic_cast<TypeId*>(ret_type))
                    {
                        if (ti->type_ == "integer")
                        {
                            var_id_access->type_ = Expr::Type::INT;
                        }
                        else if (ti->type_ == "char")
                        {
                            var_id_access->type_ = Expr::Type::STRING;
                        }
                    }
                    else if (TypeArray* ta = dynamic_cast<TypeArray*>(ret_type))
                    {
                        if (TypeId* ti =
                            dynamic_cast<TypeId*>(ta->type_denoter_))
                        {
                            if (ti->type_ == "integer")
                            {
                                var_id_access->type_ = Expr::Type::INT;
                            }
                            else if (ti->type_ == "char")
                            {
                                var_id_access->type_ = Expr::Type::STRING;
                            }
                        }
                    }

                    return;
                }
            }
            else if (p->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
            {
                if (p->proc_decl_->heading_->name_->name_ == var_id_access->id_)
                {
                    var_id_access->type_ = Expr::Type::VOID;
                    return;
                }
            }
        }
    }
}

void pascal2java::TreeAttributingVisitor::visit(
    IndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    if (indexed_variable->var_access_->type_ == Expr::Type::STRING)
    {
        indexed_variable->type_ = Expr::Type::STRING;
    }
    else if (indexed_variable->var_access_->type_ == Expr::Type::INT)
    {
        indexed_variable->type_ = Expr::Type::INT;
    }

    for (auto e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    if (indexed_variable->var_access_->type_ == Expr::Type::STRING)
    {
        indexed_variable->type_ = Expr::Type::STRING;
    }
    else if (indexed_variable->var_access_->type_ == Expr::Type::INT)
    {
        indexed_variable->type_ = Expr::Type::INT;
    }

    for (auto e : indexed_variable->expr_list_)
    {
        e->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(AssignmentStatement* statement)
{
    statement->var_access_->accept(*this);
    statement->expr_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(ProcedureStatement* statement)
{
    for (ActualParameter* p : statement->params_)
    {
        p->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(CaseStatement* statement)
{
    statement->expr_->accept(*this);

    for (CaseListElement* e : statement->elems_)
    {
        e->accept(*this);
    }
}

void pascal2java::TreeAttributingVisitor::visit(CaseListElement* case_list_elem)
{
    for (Constant* c : case_list_elem->constants_)
    {
        c->accept(*this);
    }

    case_list_elem->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(RepeatStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(
    IfWithoutElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(IfElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
    statement->else_stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(WhileStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(ForToStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(ForDowntoStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(EqualExpr* equal_expr)
{
    equal_expr->type_ = Expr::Type::BOOL;
    equal_expr->left_->accept(*this);
    equal_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(NotEqualExpr* not_equal_expr)
{
    not_equal_expr->type_ = Expr::Type::BOOL;
    not_equal_expr->left_->accept(*this);
    not_equal_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(LtExpr* lt_expr)
{
    lt_expr->type_ = Expr::Type::BOOL;
    lt_expr->left_->accept(*this);
    lt_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(GtExpr* gt_expr)
{
    gt_expr->type_ = Expr::Type::BOOL;
    gt_expr->left_->accept(*this);
    gt_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(LeExpr* le_expr)
{
    le_expr->type_ = Expr::Type::BOOL;
    le_expr->left_->accept(*this);
    le_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(GeExpr* ge_expr)
{
    ge_expr->type_ = Expr::Type::BOOL;
    ge_expr->left_->accept(*this);
    ge_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(AndExpr* and_expr)
{
    and_expr->type_ = Expr::Type::BOOL;
    and_expr->left_->accept(*this);
    and_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(OrExpr* or_expr)
{
    or_expr->type_ = Expr::Type::BOOL;
    or_expr->left_->accept(*this);
    or_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(NotExpr* not_expr)
{
    not_expr->type_ = Expr::Type::BOOL;
    not_expr->expr_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(PlusExpr* plus_expr)
{
    plus_expr->type_ = Expr::Type::INT;
    plus_expr->left_->accept(*this);
    plus_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(MinusExpr* minus_expr)
{
    minus_expr->type_ = Expr::Type::INT;
    minus_expr->left_->accept(*this);
    minus_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(StarExpr* star_expr)
{
    star_expr->type_ = Expr::Type::INT;
    star_expr->left_->accept(*this);
    star_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(SlashExpr* slash_expr)
{
    slash_expr->type_ = Expr::Type::INT;
    slash_expr->left_->accept(*this);
    slash_expr->right_->accept(*this);
}

void pascal2java::TreeAttributingVisitor::visit(DivExpr* div_expr)
{
}

void pascal2java::TreeAttributingVisitor::visit(ModExpr* mod_expr)
{
}

void pascal2java::TreeAttributingVisitor::visit(UnaryMinus* minus)
{
    minus->expr_->accept(*this);
    minus->type_ = minus->expr_->type_;
}

void pascal2java::TreeAttributingVisitor::visit(Constant* constant)
{
    if (constant->const_type() == Constant::Type::NUMBER)
    {
        constant->type_ = Expr::Type::INT;
    }
    else if (constant->const_type() == Constant::Type::STRING)
    {
        constant->type_ = Expr::Type::STRING;
    }
    else if (constant->const_type() == Constant::Type::ID)
    {
        constant->type_ = Expr::Type::NOT_INIT;

        for (auto c : constants_)
        {
            if (c.first == constant->get_string_or_id())
            {
                constant->type_ = c.second->type_;
            }
        }

        if (parent_name_ != program_name_)
        {
            for (auto c : global_constants_)
            {
                if (c.first == constant->get_string_or_id())
                {
                    constant->type_ = c.second->type_;
                }
            }
        }
    }
}
