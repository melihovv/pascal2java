﻿#ifndef DOTWRITER_H
#define DOTWRITER_H
#include <QtCore/QString>

namespace pascal2java
{
    class DotWriter
    {
    public:
        static void write(const QString& file_name, const QString& dot);
    };
}
#endif // DOTWRITER_H
