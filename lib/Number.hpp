﻿#ifndef NUMBER_H
#define NUMBER_H

#include <boost/variant.hpp>

namespace pascal2java
{
    class Number
    {
    public:
        enum class Type
        {
            DOUBLE,
            INT,
            NOT_INIT
        };

        Number()
            : type_{Type::NOT_INIT}
        {
        }

        Number(double value)
            : type_{Type::DOUBLE},
              value_{value}
        {
        }

        Number(long long value)
            : type_{Type::INT},
              value_{value}
        {
        }

        double get_double()
        {
            return boost::get<double>(value_);
        }

        long long get_int()
        {
            return boost::get<long long>(value_);
        }

        Type type() const
        {
            return type_;
        }

    private:
        Type type_;
        boost::variant<double, long long> value_;
    };
}

#endif // NUMBER_H
