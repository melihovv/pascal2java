﻿#ifndef BYTECODEGENERATORVISITOR_H
#define BYTECODEGENERATORVISITOR_H

#include <QtCore/QMap>
#include <QDataStream>
#include "ConstantTable.hpp"
#include "IVisitor.hpp"

namespace pascal2java
{
    class BytecodeGeneratorVisitor : public IVisitor
    {
    public:
        explicit BytecodeGeneratorVisitor(
            const QMap<QString, ConstantTable>& constant_tables);
        virtual void visit(Program* program) override;
        virtual void visit(Block* block) override;
        virtual void visit(TypeId* type_id) override;
        virtual void visit(TypeArray* type_array) override;
        virtual void visit(SubrangeType* subrange_type) override;
        virtual void visit(FunctionDeclaration* func_decl) override;
        virtual void visit(ProcedureDeclaration* proc_decl) override;
        virtual void visit(ValueParameterSpecification* val_par_spec) override;
        virtual void visit(VariableParameterSpecification* var_par_spec)
            override;
        virtual void visit(FunctionHeading* func_heading) override;
        virtual void visit(FormalParameterSection* formal_param_sec) override;
        virtual void visit(ProcedureIdentification* proc_ident) override;
        virtual void visit(FunctionIdentification* func_ident) override;
        virtual void visit(ProcedureHeading* proc_heading) override;
        virtual void visit(ProcOrFuncDeclaration* proc_or_func_decl) override;
        virtual void visit(ActualParameter* actual_parameter) override;
        virtual void visit(FunctionDesignator* func_designator) override;
        virtual void visit(VariableIdAccess* var_id_access) override;
        virtual void visit(IndexedVariable* indexed_variable) override;
        virtual void visit(WriteToIndexedVariable* indexed_variable) override;
        virtual void visit(AssignmentStatement* statement) override;
        virtual void visit(ProcedureStatement* statement) override;
        virtual void visit(CompoundStatement* statement) override;
        virtual void visit(CaseStatement* statement) override;
        virtual void visit(CaseListElement* case_list_elem) override;
        virtual void visit(RepeatStatement* statement) override;
        virtual void visit(IfWithoutElseStatement* statement) override;
        virtual void visit(IfElseStatement* statement) override;
        virtual void visit(WhileStatement* statement) override;
        virtual void visit(ForToStatement* statement) override;
        virtual void visit(ForDowntoStatement* statement) override;
        virtual void visit(EqualExpr* equal_expr) override;
        virtual void visit(NotEqualExpr* not_equal_expr) override;
        virtual void visit(LtExpr* lt_expr) override;
        virtual void visit(GtExpr* gt_expr) override;
        virtual void visit(LeExpr* le_expr) override;
        virtual void visit(GeExpr* ge_expr) override;
        virtual void visit(AndExpr* and_expr) override;
        virtual void visit(OrExpr* or_expr) override;
        virtual void visit(NotExpr* not_expr) override;
        virtual void visit(PlusExpr* plus_expr) override;
        virtual void visit(MinusExpr* minus_expr) override;
        virtual void visit(StarExpr* star_expr) override;
        virtual void visit(SlashExpr* slash_expr) override;
        virtual void visit(DivExpr* div_expr) override;
        virtual void visit(ModExpr* mod_expr) override;
        virtual void visit(UnaryMinus* minus) override;
        virtual void visit(Constant* constant) override;

    private:
        void write_u4(int u4, bool to_buffer = false);
        void write_u2(ushort u2, bool to_buffer = false);
        void write_s2(short s2, bool to_buffer = false);
        void write_u1(uchar u1, bool to_buffer = false);
        void write_string(const QString& s, bool to_buffer = false);
        void write_intro();
        void write_constant_table();
        void write_class_metadata();
        void write_field_table();
        void write_method_table();
        void write_default_constructor();
        void write_main_method();
        void write_class_attributes();
        bool is_global() const;

        const QMap<QString, ConstantTable> constants_;
        QDataStream out_;
        QString program_name_;
        QString parent_name_;
        Block* main_block_;
        QList<ProcOrFuncDeclaration*> proc_and_func_;
        QByteArray buffer_;
        Expr* var_id_access_;

        const uint MAGIC_NUMBER_ = 0xCAFEBABE;
        const uint MAJOR_JDK_VERSION_ = 0x2d;
        const uint MINOR_JDK_VERSION_ = 0x3;

        const int UTF8_TYPE_ = 1;
        const int INTEGER_TYPE_ = 3;
        const int STRING_TYPE_ = 8;
        const int NAME_AND_TYPE_TYPE_ = 12;
        const int CLASS_TYPE_ = 7;
        const int FIELD_REF_TYPE_ = 9;
        const int METHOD_REF_TYPE_ = 10;

        const ushort ACC_SUPER = 0x20;
        const ushort ACC_PUBLIC = 0x1;
        const ushort ACC_STATIC = 0x8;

        const uchar ICONST_M1_ = 0x2;
        const uchar ICONST_0_ = 0x3;
        const uchar ICONST_1_ = 0x4;
        const uchar ICONST_2_ = 0x5;
        const uchar ICONST_3_ = 0x6;
        const uchar ICONST_4_ = 0x7;
        const uchar ICONST_5_ = 0x8;

        const uchar BIPUSH_ = 0x10;
        const uchar SIPUSH_ = 0x11;

        const uchar LDC_ = 0x12;
        const uchar LDC_W_ = 0x13;

        const uchar ILOAD_ = 0x15;
        const uchar FLOAD_ = 0x17;
        const uchar ALOAD_ = 0x19;
        const uchar ALOAD_0_ = 0x2a;

        const uchar ISTORE_ = 0x36;
        const uchar FSTORE_ = 0x38;
        const uchar ASTORE_ = 0x3A;

        const uchar POP_ = 0x57;

        const uchar DUP_ = 0x58;
        const uchar DUP2_ = 0x5C;

        const uchar IADD_ = 0x60;
        const uchar FADD_ = 0x62;

        const uchar IMUL_ = 0x68;
        const uchar FMUL_ = 0x6A;

        const uchar ISUB_ = 0x64;
        const uchar FSUB_ = 0x66;

        const uchar IDIV_ = 0x6C;
        const uchar FDIV_ = 0x6e;

        const uchar IINC_ = 0x84;

        const uchar FNEG_ = 0x76;
        const uchar INEG_ = 0x74;

        const uchar IREM_ = 0x70;

        const uchar IF_ICMPEQ_ = 0x9F;
        const uchar IF_ICMPNE_ = 0xA0;
        const uchar IF_ICMPLT_ = 0xA1;
        const uchar IF_ICMPLE_ = 0xA4;
        const uchar IF_ICMPGT_ = 0xA3;
        const uchar IF_ICMPGE_ = 0xA2;

        const uchar FCMPG_ = 0x96;
        const uchar FCMPL_ = 0x95;

        const uchar IFEQ_ = 0x99;
        const uchar IFNE_ = 0x9A;
        const uchar IFLT_ = 0x55;
        const uchar IFLE_ = 0x9E;
        const uchar IFGT_ = 0x9D;
        const uchar IFGE_ = 0x9C;

        const uchar IF_ACMPEQ_ = 0xA5;
        const uchar IF_ACMPNE_ = 0xA6;

        const uchar GOTO_ = 0xA7;
        const uchar TABLESWITCH_ = 0xAA;
        const uchar LOOKUPSWITCH_ = 0xAB;

        const uchar NEWARRAY_ = 0xBC;
        const uchar T_BOOLEAN_ = 4;
        const uchar T_CHAR_ = 5;
        const uchar T_FLOAT_ = 6;
        const uchar T_DOUBLE_ = 7;
        const uchar T_BYTE_ = 8;
        const uchar T_SHORT_ = 9;
        const uchar T_INT_ = 10;
        const uchar T_LONG_ = 11;
        const uchar ANEWARRAY_ = 0xBD;
        const uchar ARRAY_LENGTH_ = 0xBE;
        const uchar IALOAD_ = 0x2E;
        const uchar AALOAD_ = 0x32;
        const uchar FALOAD_ = 0x30;
        const uchar IASTORE_ = 0x4F;
        const uchar AASTORE_ = 0x53;
        const uchar FASTORE_ = 0x51;

        const uchar NEW_ = 0xBB;
        const uchar GETSTATIC_ = 0xB2;
        const uchar PUTSTATIC_ = 0xB3;
        const uchar GETFIELD_ = 0xB4;
        const uchar PUTFIELD_ = 0xB5;
        const uchar INSTANCE_ = 0xC1;

        const uchar INVOKEVIRTUAL_ = 0xB6;
        const uchar INVOKESPECIAL_ = 0xB7;
        const uchar INVOKESTATIC_ = 0xB8;
        const uchar IRETURN_ = 0xAC;
        const uchar FRETURN_ = 0xAE;
        const uchar ARETURN_ = 0xB0;
        const uchar RETURN_ = 0xB1;
    };
}

#endif // BYTECODEGENERATORVISITOR_H
