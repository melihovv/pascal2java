﻿#include "DotGeneratorVisitor.hpp"
#include <iostream>
#include "../lib/Ast.hpp"
#include <QtCore/QString>

void pascal2java::DotGeneratorVisitor::visit(Program* program)
{
    dot_ += "graph program {\n";
    block_parent_ = "program";
    visit(program->block_);
    dot_ += "\n}";
}

void pascal2java::DotGeneratorVisitor::visit(Block* block)
{
    if (block->constants_.size() > 0)
    {
        dot_ += block_parent_ + "--q" + QString::number(counter_q_) + "--";

        for (const std::pair<QString, Expr*>& constant : block->constants_)
        {
            dot_ += "r" + QString::number(counter_r_) +
                "--aa" + QString::number(counter_aa_);
            dot_ += "\n";

            dot_ += "aa" + QString::number(counter_aa_) +
                QString(" [label=\"" + constant.first + "\"]\n");

            dot_ += "aa" + QString::number(counter_aa_++) + "--";
            constant.second->accept(*this);
        }

        dot_ += "r" + QString::number(counter_r_++) +
            QString(" [label=\"ConstantDefinitionList\"]\n");

        dot_ += "q" + QString::number(counter_q_++) +
            QString(" [label=\"ConstantDefinitionPart\"]\n");
    }

    if (block->vars_.size() > 0)
    {
        dot_ += block_parent_ + "--s" + QString::number(counter_s_) + "--";

        for (const auto& key : block->vars_)
        {
            dot_ += "t" + QString::number(counter_t_) + "--g" +
                QString::number(counter_g_) + QString("--") +
                "gg" + QString::number(counter_gg_);
            dot_ += "\n";

            dot_ += "gg" + QString::number(counter_gg_++) +
                QString(" [label=\"" + key.first + "\"]\n");

            dot_ += "g" + QString::number(counter_g_) + "--";
            key.second->accept(*this);
            dot_ += "\n";

            dot_ += "g" + QString::number(counter_g_++) +
                QString(" [label=\"VariableDeclaration\"]\n");
        }

        dot_ += "t" + QString::number(counter_t_++) +
            QString(" [label=\"VariableDeclarationList\"]\n");

        dot_ += "s" + QString::number(counter_s_++) +
            QString(" [label=\"VariableDeclarationPart\"]\n");
    }

    if (block->proc_and_func_.size() > 0)
    {
        dot_ += block_parent_ + "--u" + QString::number(counter_u_) + "--";

        for (auto f : block->proc_and_func_)
        {
            dot_ += "v" + QString::number(counter_v_) + "--";

            f->accept(*this);
            dot_ += "\n";
        }

        dot_ += "v" + QString::number(counter_v_++) +
            QString(" [label=\"ProcOrFuncDeclarationList\"]\n");

        dot_ += "u" + QString::number(counter_u_++) +
            QString(" [label=\"ProcOrFuncDeclarationPart\"]\n");
    }

    dot_ += block_parent_ + "--x" + QString::number(counter_x_) + "\n";

    QString tmp = stmt_parent_;
    stmt_parent_ = "x" + QString::number(counter_x_);
    block->stmt_->accept(*this);
    stmt_parent_ = tmp;

    dot_ += "x" + QString::number(counter_x_++) +
        QString(" [label=\"StatementPart\"]\n");
}

void pascal2java::DotGeneratorVisitor::visit(TypeId* type_id)
{
    dot_ += "e" + QString::number(counter_e_) + "\n";
    dot_ += "e" + QString::number(counter_e_) +
        " [label=\"" + type_id->type_ + "\"]\n";
    ++counter_e_;
}

void pascal2java::DotGeneratorVisitor::visit(TypeArray* type_array)
{
    dot_ += "f" + QString::number(counter_f_) + "--";

    for (auto l : type_array->index_type_list_)
    {
        l->accept(*this);
    }

    dot_ += "f" + QString::number(counter_f_) + "--";
    type_array->type_denoter_->accept(*this);

    dot_ += "f" + QString::number(counter_f_) +
        " [label=\"ArrayType\"]\n";
    ++counter_f_;
}

void pascal2java::DotGeneratorVisitor::visit(SubrangeType* subrange_type)
{
    subrange_type->min()->accept(*this);
    dot_ += "f" + QString::number(counter_f_) + "--";
    subrange_type->max()->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }
    else
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }
}

void pascal2java::DotGeneratorVisitor::visit(
    ProcedureHeading* proc_heading)
{
    int node_id = counter_o_;

    dot_ += "o" + QString::number(counter_o_++) + "--";
    proc_heading->name_->accept(*this);

    for (auto p : proc_heading->params_)
    {
        dot_ += "o" + QString::number(node_id) + "--";
        p->accept(*this);
    }

    dot_ += "o" + QString::number(node_id) + " [label=\"ProcedureHeading\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    FunctionIdentification* func_ident)
{
    dot_ += "j" + QString::number(counter_j_++) + "--";
    dot_ += "j" + QString::number(counter_j_++) + "\n";

    dot_ += "j" + QString::number(counter_j_ - 1) +
        " [label=\"" + func_ident->name_ + "\"]\n";
    dot_ += "j" + QString::number(counter_j_ - 2) +
        " [label=\"FunctionIdentification\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    ProcedureIdentification* proc_ident)
{
    dot_ += "p" + QString::number(counter_p_++) + "--";
    dot_ += "p" + QString::number(counter_p_++) + "\n";

    dot_ += "p" + QString::number(counter_p_ - 1) +
        " [label=\"" + proc_ident->name_ + "\"]\n";
    dot_ += "p" + QString::number(counter_p_ - 2) +
        " [label=\"ProcedureIdentification\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
    int node_id = counter_k_;
    dot_ += "k" + QString::number(counter_k_++) + "\n";

    if (formal_param_sec->type() == FormalParameterSection::Type::VALUE)
    {
        formal_param_sec->val_param_spec_->accept(*this);
    }
    else
    {
        formal_param_sec->var_param_spec_->accept(*this);
    }

    dot_ += "k" + QString::number(node_id) +
        " [label=\"FormalParameterList\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    FunctionHeading* func_heading)
{
    int node_id = counter_i_;

    dot_ += "i" + QString::number(counter_i_++) + "--";
    func_heading->name_->accept(*this);

    for (auto p : func_heading->params_)
    {
        dot_ += "i" + QString::number(node_id) + "--";
        p->accept(*this);
    }

    dot_ += "i" + QString::number(node_id) + "--";
    func_heading->return_type_->accept(*this);

    dot_ += "i" + QString::number(node_id) + " [label=\"FunctionHeading\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    VariableParameterSpecification* value_par_spec)
{
    for (const auto& key : value_par_spec->vars_)
    {
        dot_ += "k" + QString::number(counter_k_ - 1) + "--m" +
            QString::number(counter_m_) + QString("--") + key.first;
        dot_ += "\n";

        dot_ += "m" + QString::number(counter_m_) + "--";
        key.second->accept(*this);
        dot_ += "\n";

        dot_ += "m" + QString::number(counter_m_++) +
            " [label=\"VariableParameterSpecification\"]\n";
    }
}

void pascal2java::DotGeneratorVisitor::visit(
    ValueParameterSpecification* value_par_spec)
{
    for (const auto& key : value_par_spec->vars_)
    {
        dot_ += "k" + QString::number(counter_k_ - 1) + "--l" +
            QString::number(counter_l_) + QString("--") + key.first;
        dot_ += "\n";

        dot_ += "l" + QString::number(counter_l_) + "--";
        key.second->accept(*this);
        dot_ += "\n";

        dot_ += "l" + QString::number(counter_l_++) +
            " [label=\"ValueParameterSpecification\"]\n";
    }
}

void pascal2java::DotGeneratorVisitor::visit(
    FunctionDeclaration* func_decl)
{
    int node_id = counter_h_;

    dot_ += "h" + QString::number(counter_h_++) + "--";
    func_decl->heading_->accept(*this);

    QString temp = block_parent_;
    block_parent_ = "h" + QString::number(counter_n_++);
    func_decl->body_->accept(*this);
    block_parent_ = temp;

    if (func_decl->is_forward_)
    {
        dot_ += "h" + QString::number(node_id) + "--" + "h" +
            QString::number(counter_h_) + "\n";
        dot_ += "h" + QString::number(counter_h_++) +
            QString(" [label=\"forward\"]\n");
    }

    dot_ += "h" + QString::number(node_id) +
        " [label=\"FunctionDeclaration\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    ProcedureDeclaration* proc_decl)
{
    int node_id = counter_n_;

    dot_ += "n" + QString::number(counter_n_) + "--";
    proc_decl->heading_->accept(*this);

    QString temp = block_parent_;
    block_parent_ = "n" + QString::number(counter_n_++);
    proc_decl->body_->accept(*this);
    block_parent_ = temp;

    if (proc_decl->is_forward_)
    {
        dot_ += "n" + QString::number(node_id) + "--" +
            "n" + QString::number(counter_n_) + "\n";
        dot_ += "n" + QString::number(counter_n_++) +
            QString(" [label=\"forward\"]\n");
    }

    dot_ += "n" + QString::number(node_id) +
        " [label=\"ProcedureDeclaration\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    ActualParameter* actual_parameter)
{
    dot_ += "ff" + QString::number(counter_ff_) + "\n";
    dot_ += "ff" + QString::number(counter_ff_) +
        " [label=\"ActualParameter\"]\n";

    dot_ += "ff" + QString::number(counter_ff_++) + "--";
    actual_parameter->expr_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    FunctionDesignator* func_designator)
{
    dot_ += "dd" + QString::number(counter_dd_) + "\n";
    dot_ += "dd" + QString::number(counter_dd_) +
        " [label=\"FuntionDesignator (" + func_designator->type() + ")\"]\n";

    dot_ += "dd" + QString::number(counter_dd_) + "--" +
        "ee" + QString::number(counter_ee_) + "\n";
    dot_ += "ee" + QString::number(counter_ee_++) +
        " [label=\"" + func_designator->id_ + "\"]\n";

    for (ActualParameter* p : func_designator->params_)
    {
        dot_ += "dd" + QString::number(counter_dd_) + "--";
        p->accept(*this);
    }

    ++counter_dd_;
}

void pascal2java::DotGeneratorVisitor::visit(
    AssignmentStatement* statement)
{
    dot_ += stmt_parent_ + "--y" + QString::number(counter_y_) + "\n";
    dot_ += "y" + QString::number(counter_y_) +
        " [label=\"AssignmentStatement\"]\n";

    dot_ += "y" + QString::number(counter_y_) + "--";
    statement->var_access_->accept(*this);

    dot_ += "y" + QString::number(counter_y_++) + "--";
    statement->expr_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    VariableIdAccess* var_id_access)
{
    dot_ += "z" + QString::number(counter_z_) + "\n";
    dot_ += "z" + QString::number(counter_z_) +
        " [label=\"VariableIdAccess (" + var_id_access->type() + ")\"]\n";

    dot_ += "z" + QString::number(counter_z_++) + "--" +
        "cc" + QString::number(counter_cc_) + "\n";
    dot_ += "cc" + QString::number(counter_cc_++) +
        " [label=\"" + var_id_access->id_ + "\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(
    IndexedVariable* indexed_variable)
{
    dot_ += "bb" + QString::number(counter_bb_) + "\n";
    dot_ += "bb" + QString::number(counter_bb_) +
        " [label=\"IndexedVariable (" + indexed_variable->type() + ")\"]\n";

    for (Expr* e : indexed_variable->expr_list_)
    {
        dot_ += "bb" + QString::number(counter_bb_) + "--";
        e->accept(*this);
    }

    dot_ += "bb" + QString::number(counter_bb_++) + "--";
    indexed_variable->var_access_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
    dot_ += "iii" + QString::number(counter_iii_) + "\n";
    dot_ += "iii" + QString::number(counter_iii_) +
        " [label=\"WriteToIndexedVariable (" + indexed_variable->type() +
        ")\"]\n";

    for (Expr* e : indexed_variable->expr_list_)
    {
        dot_ += "iii" + QString::number(counter_iii_) + "--";
        e->accept(*this);
    }

    dot_ += "iii" + QString::number(counter_iii_++) + "--";
    indexed_variable->var_access_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    ProcedureStatement* statement)
{
    dot_ += stmt_parent_ + "--uu" + QString::number(counter_uu_) + "\n";
    dot_ += "uu" + QString::number(counter_uu_) +
        " [label=\"ProcedureStatement\"]\n";

    dot_ += "uu" + QString::number(counter_uu_) + "--vv" +
        QString::number(counter_vv_) + "\n";
    dot_ += "vv" + QString::number(counter_vv_++) +
        " [label=\"" + statement->id_ + "\"]\n";

    for (ActualParameter* p : statement->params_)
    {
        dot_ += "uu" + QString::number(counter_uu_) + "--";
        p->accept(*this);
    }

    ++counter_uu_;
}

void pascal2java::DotGeneratorVisitor::visit(
    CompoundStatement* statement)
{
    QString tmp = stmt_parent_;
    dot_ += stmt_parent_ + "--hhh" + QString::number(counter_hhh_) + "\n";
    dot_ += "hhh" + QString::number(counter_hhh_) +
        " [label=\"CompoundStatement\"]\n";
    stmt_parent_ = "hhh" + QString::number(counter_hhh_++);

    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }

    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(CaseStatement* statement)
{
    dot_ += stmt_parent_ + "--fff" + QString::number(counter_fff_) + "\n";
    dot_ += "fff" + QString::number(counter_fff_) +
        " [label=\"CaseStatement\"]\n";

    dot_ += "fff" + QString::number(counter_fff_) + "--";
    statement->expr_->accept(*this);

    for (CaseListElement* e : statement->elems_)
    {
        dot_ += "fff" + QString::number(counter_fff_) + "--";
        e->accept(*this);
    }

    ++counter_fff_;
}

void pascal2java::DotGeneratorVisitor::visit(
    CaseListElement* case_list_elem)
{
    dot_ += "ggg" + QString::number(counter_ggg_) + "\n";
    dot_ += "ggg" + QString::number(counter_ggg_) +
        " [label=\"CaseListElement\"]\n";

    for (Constant* c : case_list_elem->constants_)
    {
        dot_ += "ggg" + QString::number(counter_ggg_) + "--";
        c->accept(*this);
    }

    QString tmp = stmt_parent_;
    stmt_parent_ = "ggg" + QString::number(counter_ggg_);
    case_list_elem->stmt_->accept(*this);
    stmt_parent_ = tmp;

    ++counter_ggg_;
}

void pascal2java::DotGeneratorVisitor::visit(RepeatStatement* statement)
{
    dot_ += stmt_parent_ + "--zz" + QString::number(counter_zz_) + "\n";
    dot_ += "zz" + QString::number(counter_zz_) +
        " [label=\"RepeatStatement\"]\n";

    dot_ += "zz" + QString::number(counter_zz_) + "--";
    statement->expr_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "zz" + QString::number(counter_zz_++);
    statement->stmt_->accept(*this);
    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(
    IfWithoutElseStatement* statement)
{
    dot_ += stmt_parent_ + "--xx" + QString::number(counter_xx_) + "\n";
    dot_ += "xx" + QString::number(counter_xx_) +
        " [label=\"IfStatement\"]\n";

    dot_ += "xx" + QString::number(counter_xx_) + "--";
    statement->condition_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "xx" + QString::number(counter_xx_++);
    statement->stmt_->accept(*this);
    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(IfElseStatement* statement)
{
    dot_ += stmt_parent_ + "--yy" + QString::number(counter_yy_) + "\n";
    dot_ += "yy" + QString::number(counter_yy_) +
        " [label=\"IfElseStatement\"]\n";

    dot_ += "yy" + QString::number(counter_yy_) + "--";
    statement->condition_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "yy" + QString::number(counter_yy_);
    statement->stmt_->accept(*this);

    stmt_parent_ = "yy" + QString::number(counter_yy_++);
    statement->else_stmt_->accept(*this);

    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(WhileStatement* statement)
{
    dot_ += stmt_parent_ + "--aaa" + QString::number(counter_aaa_) + "\n";
    dot_ += "aaa" + QString::number(counter_aaa_) +
        " [label=\"WhileStatement\"]\n";

    dot_ += "aaa" + QString::number(counter_aaa_) + "--";
    statement->expr_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "aaa" + QString::number(counter_aaa_++);
    statement->stmt_->accept(*this);
    stmt_parent_ = tmp;

}

void pascal2java::DotGeneratorVisitor::visit(ForToStatement* statement)
{
    dot_ += stmt_parent_ + "--bbb" + QString::number(counter_bbb_) + "\n";
    dot_ += "bbb" + QString::number(counter_bbb_) +
        " [label=\"ForToStatement\"]\n";

    dot_ += "bbb" + QString::number(counter_bbb_) + "--ccc" +
        QString::number(counter_ccc_) + "\n";
    dot_ += "ccc" + QString::number(counter_ccc_) +
        " [label=\"" + statement->id_ + "\"]\n";

    dot_ += "bbb" + QString::number(counter_bbb_) + "--";
    statement->from_->accept(*this);

    dot_ += "bbb" + QString::number(counter_bbb_) + "--";
    statement->to_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "bbb" + QString::number(counter_bbb_++);
    statement->stmt_->accept(*this);
    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(
    ForDowntoStatement* statement)
{
    dot_ += stmt_parent_ + "--ddd" + QString::number(counter_ddd_) + "\n";
    dot_ += "ddd" + QString::number(counter_ddd_) +
        " [label=\"ForDowntoStatement\"]\n";

    dot_ += "ddd" + QString::number(counter_ddd_) + "--eee" +
        QString::number(counter_eee_) + "\n";
    dot_ += "eee" + QString::number(counter_eee_) +
        " [label=\"" + statement->id_ + "\"]\n";

    dot_ += "ddd" + QString::number(counter_ddd_) + "--";
    statement->from_->accept(*this);

    dot_ += "ddd" + QString::number(counter_ddd_) + "--";
    statement->to_->accept(*this);

    QString tmp = stmt_parent_;
    stmt_parent_ = "ddd" + QString::number(counter_ddd_++);
    statement->stmt_->accept(*this);
    stmt_parent_ = tmp;
}

void pascal2java::DotGeneratorVisitor::visit(EqualExpr* equal_expr)
{
    dot_ += "ii" + QString::number(counter_ii_) + "\n";
    dot_ += "ii" + QString::number(counter_ii_) +
        " [label=\"EqualExpr (" + equal_expr->type() + ")\"]\n";

    dot_ += "ii" + QString::number(counter_ii_) + "--";
    equal_expr->left_->accept(*this);

    dot_ += "ii" + QString::number(counter_ii_++) + "--";
    equal_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(
    NotEqualExpr* not_equal_expr)
{
    dot_ += "jj" + QString::number(counter_jj_) + "\n";
    dot_ += "jj" + QString::number(counter_jj_) +
        " [label=\"NotEqualExpr (" + not_equal_expr->type() + ")\"]\n";

    dot_ += "jj" + QString::number(counter_jj_) + "--";
    not_equal_expr->left_->accept(*this);

    dot_ += "jj" + QString::number(counter_jj_++) + "--";
    not_equal_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(LtExpr* lt_expr)
{
    dot_ += "kk" + QString::number(counter_kk_) + "\n";
    dot_ += "kk" + QString::number(counter_kk_) +
        " [label=\"LtExpr (" + lt_expr->type() + ")\"]\n";

    dot_ += "kk" + QString::number(counter_kk_) + "--";
    lt_expr->left_->accept(*this);

    dot_ += "kk" + QString::number(counter_kk_++) + "--";
    lt_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(GtExpr* gt_expr)
{
    dot_ += "ll" + QString::number(counter_ll_) + "\n";
    dot_ += "ll" + QString::number(counter_ll_) +
        " [label=\"GtExpr (" + gt_expr->type() + ")\"]\n";

    dot_ += "ll" + QString::number(counter_ll_) + "--";
    gt_expr->left_->accept(*this);

    dot_ += "ll" + QString::number(counter_ll_++) + "--";
    gt_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(LeExpr* le_expr)
{
    dot_ += "mm" + QString::number(counter_mm_) + "\n";
    dot_ += "mm" + QString::number(counter_mm_) +
        " [label=\"LeExpr (" + le_expr->type() + ")\"]\n";

    dot_ += "mm" + QString::number(counter_mm_) + "--";
    le_expr->left_->accept(*this);

    dot_ += "mm" + QString::number(counter_mm_++) + "--";
    le_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(GeExpr* ge_expr)
{
    dot_ += "nn" + QString::number(counter_nn_) + "\n";
    dot_ += "nn" + QString::number(counter_nn_) +
        " [label=\"GeExpr (" + ge_expr->type() + ")\"]\n";

    dot_ += "nn" + QString::number(counter_nn_) + "--";
    ge_expr->left_->accept(*this);

    dot_ += "nn" + QString::number(counter_nn_++) + "--";
    ge_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(AndExpr* and_expr)
{
    dot_ += "pp" + QString::number(counter_pp_) + "\n";
    dot_ += "pp" + QString::number(counter_pp_) +
        " [label=\"AndExpr (" + and_expr->type() + ")\"]\n";

    dot_ += "pp" + QString::number(counter_pp_) + "--";
    and_expr->left_->accept(*this);

    dot_ += "pp" + QString::number(counter_pp_++) + "--";
    and_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(OrExpr* or_expr)
{
    dot_ += "oo" + QString::number(counter_oo_) + "\n";
    dot_ += "oo" + QString::number(counter_oo_) +
        " [label=\"OrExpr (" + or_expr->type() + ")\"]\n";

    dot_ += "oo" + QString::number(counter_oo_) + "--";
    or_expr->left_->accept(*this);

    dot_ += "oo" + QString::number(counter_oo_++) + "--";
    or_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(NotExpr* not_expr)
{
    dot_ += "hh" + QString::number(counter_hh_) + "--";
    not_expr->expr_->accept(*this);
    dot_ += "hh" + QString::number(counter_hh_++) +
        " [label=\"NotExpr (" + not_expr->type() + ")\"]";
}

void pascal2java::DotGeneratorVisitor::visit(PlusExpr* plus_expr)
{
    dot_ += "qq" + QString::number(counter_qq_) + "\n";
    dot_ += "qq" + QString::number(counter_qq_) +
        " [label=\"PlusExpr (" + plus_expr->type() + ")\"]\n";

    dot_ += "qq" + QString::number(counter_qq_) + "--";
    plus_expr->left_->accept(*this);

    dot_ += "qq" + QString::number(counter_qq_++) + "--";
    plus_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(MinusExpr* minus_expr)
{
    dot_ += "rr" + QString::number(counter_rr_) + "\n";
    dot_ += "rr" + QString::number(counter_rr_) +
        " [label=\"MinusExpr (" + minus_expr->type() + ")\"]\n";

    dot_ += "rr" + QString::number(counter_rr_) + "--";
    minus_expr->left_->accept(*this);

    dot_ += "rr" + QString::number(counter_rr_++) + "--";
    minus_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(StarExpr* star_expr)
{
    dot_ += "ss" + QString::number(counter_ss_) + "\n";
    dot_ += "ss" + QString::number(counter_ss_) +
        " [label=\"StarExpr (" + star_expr->type() + ")\"]\n";

    dot_ += "ss" + QString::number(counter_ss_) + "--";
    star_expr->left_->accept(*this);

    dot_ += "ss" + QString::number(counter_ss_++) + "--";
    star_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(SlashExpr* slash_expr)
{
    dot_ += "tt" + QString::number(counter_tt_) + "\n";
    dot_ += "tt" + QString::number(counter_tt_) +
        " [label=\"SlashExpr (" + slash_expr->type() + ")\"]\n";

    dot_ += "tt" + QString::number(counter_tt_) + "--";
    slash_expr->left_->accept(*this);

    dot_ += "tt" + QString::number(counter_tt_++) + "--";
    slash_expr->right_->accept(*this);
}

void pascal2java::DotGeneratorVisitor::visit(DivExpr* div_expr)
{
}

void pascal2java::DotGeneratorVisitor::visit(ModExpr* mod_expr)
{
}

void pascal2java::DotGeneratorVisitor::visit(UnaryMinus* minus)
{
    dot_ += "c" + QString::number(counter_c_) + "--";
    minus->expr_->accept(*this);
    dot_ += "c" + QString::number(counter_c_++) +
        " [label=\"Minus (" + minus->type() + ")\"]\n";
}

void pascal2java::DotGeneratorVisitor::visit(Constant* constant)
{
    dot_ += "a" + QString::number(counter_a_) + "--";
    dot_ += "d" + QString::number(counter_d_) + "\n";

    QString tmp;
    Constant::Type constType = constant->const_type();
    if (constType == Constant::Type::ID ||
        constType == Constant::Type::STRING)
    {
        tmp = constant->get_string_or_id();
    }
    else if (constType == Constant::Type::NUMBER)
    {
        Number number = constant->get_number();
        Number::Type numType = number.type();

        if (numType == Number::Type::DOUBLE)
        {
            tmp = QString::number(number.get_double());
        }
        else if (numType == Number::Type::INT)
        {
            tmp = QString::number(number.get_int());
        }
    }

    dot_ += "d" + QString::number(counter_d_++) + " [label=\"" + tmp +
        " (" + constant->type() + ")\"]\n";
    dot_ += "a" + QString::number(counter_a_++) +
        QString(" [label=\"ConstantDefinition\"]\n");
}
