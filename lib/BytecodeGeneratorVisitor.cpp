﻿#include "BytecodeGeneratorVisitor.hpp"
#include "Ast.hpp"
#include <QtCore/qfile.h>

pascal2java::BytecodeGeneratorVisitor::BytecodeGeneratorVisitor(
    const QMap<QString, ConstantTable>& constant_tables)
    : constants_{constant_tables},
      main_block_{nullptr}
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(Program* program)
{
    program_name_ = program->name_;
    QString file_name{program_name_ + ".class"};
    QFile output_file{file_name};

    if (!output_file.open(QIODevice::WriteOnly))
    {
        QString message = "Couldn`t open file " + file_name;
        throw std::exception(message.toStdString().c_str());
    }

    out_.setDevice(&output_file);

    main_block_ = program->block_;
    proc_and_func_ = program->block_->proc_and_func_;

    write_intro();
    write_constant_table();
    write_class_metadata();
    write_field_table();
    write_method_table();
    write_class_attributes();
}

void pascal2java::BytecodeGeneratorVisitor::visit(Block* block)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(TypeId* type_id)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(TypeArray* type_array)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(SubrangeType* subrange_type)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    FunctionDeclaration* func_decl)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ProcedureDeclaration* proc_decl)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(FunctionHeading* func_heading)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ProcedureIdentification* proc_ident)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    FunctionIdentification* func_ident)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ProcedureHeading* proc_heading)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    FunctionDesignator* func_designator)
{
    auto li = func_designator->params_.constEnd();
    auto begin = func_designator->params_.constBegin();

    while(li != begin)
    {
        --li;
        (*li)->accept(*this);
    }

    write_u1(INVOKESTATIC_, true);

    int method;
    if (func_designator->id_ == "readln")
    {
        if (var_id_access_->type_ == Expr::Type::INT)
        {
            method = constants_[program_name_]
                .find_method_ref_by_name("scanInt");
        }
        else if (var_id_access_->type_ == Expr::Type::STRING)
        {
            method = constants_[program_name_]
                .find_method_ref_by_name("scanString");
        }
    }
    else
    {
        method = constants_[program_name_]
            .find_method_ref_by_name(func_designator->id_);
    }

    write_u2(method, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    VariableIdAccess* var_id_access)
{
    if (is_global())
    {
        write_u1(GETSTATIC_, true);
        int id = constants_[program_name_].find_field_ref(var_id_access->id_);
        write_u2(id, true);
    }
    else
    {
        int id = constants_[parent_name_].find_utf8(var_id_access->id_);

        if (id == -1)
        {
            id = constants_[program_name_].find_field_ref(var_id_access->id_);
            write_u1(GETSTATIC_, true);
            write_u2(id, true);
        }
        else
        {
            int index = constants_[parent_name_]
                .local_var_index(var_id_access->id_);

            // TODO ILOAD_ for integer.
            write_u1(ALOAD_, true);

            write_u1(index, true);
        }
    }
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    IndexedVariable* indexed_variable)
{
    write_u1(GETSTATIC_, true);

    VariableIdAccess* via =
        dynamic_cast<VariableIdAccess*>(indexed_variable->var_access_);
    int field = constants_[program_name_].find_field_ref(via->id_);
    write_u2(field, true);

    indexed_variable->expr_list_[0]->accept(*this);

     if (indexed_variable->type_ == Expr::Type::INT)
     {
         write_u1(IALOAD_, true);
     }
     else if (indexed_variable->type_ == Expr::Type::STRING)
     {
         write_u1(AALOAD_, true);
     }
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    AssignmentStatement* statement)
{
    if (VariableIdAccess* via =
        dynamic_cast<VariableIdAccess*>(statement->var_access_))
    {
        if (!is_global() && via->id_ == parent_name_)
        {
            statement->expr_->accept(*this);

            if (via->type_ == Expr::Type::INT)
            {
                write_u1(IRETURN_, true);
            }
            else if (via->type_ == Expr::Type::STRING)
            {
                write_u1(ARETURN_, true);
            }
        }
        else if (is_global())
        {
            var_id_access_ = statement->var_access_;

            statement->expr_->accept(*this);
            write_u1(PUTSTATIC_, true);
            write_u2(constants_[program_name_].find_field_ref(via->id_), true);

            var_id_access_ = nullptr;
        }
        else
        {
            var_id_access_ = statement->var_access_;

            statement->expr_->accept(*this);

            int id = constants_[parent_name_].find_utf8(via->id_);

            if (id == -1)
            {
                id = constants_[program_name_].find_field_ref(via->id_);
                write_u1(PUTSTATIC_, true);
                write_u2(id, true);
            }
            else
            {
                int index = constants_[parent_name_]
                    .local_var_index(via->id_);

                // TODO ISTORE_ for int.
                write_u1(ASTORE_, true);
                write_u1(index, true);
            }

            var_id_access_ = nullptr;
        }
    }
    else if (WriteToIndexedVariable* iv =
        dynamic_cast<WriteToIndexedVariable*>(statement->var_access_))
    {
        write_u1(GETSTATIC_, true);

        VariableIdAccess* via =
            dynamic_cast<VariableIdAccess*>(iv->var_access_);
        int field = constants_[program_name_]
            .find_field_ref(via->id_);
        write_u2(field, true);

        iv->expr_list_[0]->accept(*this);
        statement->expr_->accept(*this);

         if (iv->type_ == Expr::Type::INT)
         {
             write_u1(IASTORE_, true);
         }
         else if (iv->type_ == Expr::Type::STRING)
         {
             write_u1(AASTORE_, true);
         }
    }
}

void pascal2java::BytecodeGeneratorVisitor::visit(ProcedureStatement* statement)
{
    auto li = statement->params_.constEnd();
    auto begin = statement->params_.constBegin();

    bool is_int = false;
    int counter = 0;

    while(li != begin)
    {
        --li;

        if (counter == 0)
        {
            if ((*li)->expr_->type_ == Expr::Type::INT)
            {
                is_int = true;
            }
        }
        else
        {
            ++counter;
        }

        (*li)->accept(*this);
    }

    if (statement->id_ == "writeln")
    {
        write_u1(INVOKESTATIC_, true);
        int name;
        int type;

        if (is_int)
        {
            name = constants_[program_name_].find_utf8("printInt");
            type = constants_[program_name_].find_utf8("(I)V");
        }
        else
        {
            name = constants_[program_name_].find_utf8("printString");
            type = constants_[program_name_].find_utf8("(Ljava/lang/String;)V");
        }

        int nat = constants_[program_name_].find_name_and_type(name, type);
        write_u2(
            constants_[program_name_]
                .find_method_ref_id_by_name_and_type(nat),
            true);
    }
    else
    {
        write_u1(INVOKESTATIC_, true);

        int method = constants_[program_name_]
            .find_method_ref_by_name(statement->id_);
        write_u2(method, true);
    }
}

void pascal2java::BytecodeGeneratorVisitor::visit(CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::BytecodeGeneratorVisitor::visit(CaseStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    CaseListElement* case_list_elem)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(RepeatStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(
    IfWithoutElseStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(IfElseStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(WhileStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(ForToStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(ForDowntoStatement* statement)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(EqualExpr* equal_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(NotEqualExpr* not_equal_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(LtExpr* lt_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(GtExpr* gt_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(LeExpr* le_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(GeExpr* ge_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(AndExpr* and_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(OrExpr* or_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(NotExpr* not_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(PlusExpr* plus_expr)
{
    plus_expr->left_->accept(*this);
    plus_expr->right_->accept(*this);
    write_u1(IADD_, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(MinusExpr* minus_expr)
{
    minus_expr->left_->accept(*this);
    minus_expr->right_->accept(*this);
    write_u1(ISUB_, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(StarExpr* star_expr)
{
    star_expr->left_->accept(*this);
    star_expr->right_->accept(*this);
    write_u1(IMUL_, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(SlashExpr* slash_expr)
{
    slash_expr->left_->accept(*this);
    slash_expr->right_->accept(*this);
    write_u1(IDIV_, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(DivExpr* div_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(ModExpr* mod_expr)
{
}

void pascal2java::BytecodeGeneratorVisitor::visit(UnaryMinus* minus)
{
    minus->expr_->accept(*this);
    write_u1(ICONST_M1_, true);
    write_u1(IMUL_, true);
}

void pascal2java::BytecodeGeneratorVisitor::visit(Constant* constant)
{
    switch (constant->const_type())
    {
    case Constant::Type::STRING:
    {
        write_u1(LDC_, true);

        int utf8 = constants_[program_name_]
            .find_utf8(constant->get_string_or_id());
        write_u1(constants_[program_name_].find_string(utf8), true);
        break;
    }
    case Constant::Type::NUMBER:
    {
        write_u1(SIPUSH_, true);

        int n = constant->get_number().get_int();
        write_s2(n, true);
        break;
    }
    // TODO
//    case Constant::Type::ID:
//        break;
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_u4(
    int u4,
    bool to_buffer /*= false*/)
{
    if (to_buffer)
    {
        QDataStream s{&buffer_, QIODevice::Append | QIODevice::Unbuffered};
        s << u4;
    }
    else
    {
        out_ << u4;
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_u2(
    ushort u2,
    bool to_buffer /*= false*/)
{
    if (to_buffer)
    {
        QDataStream s{&buffer_, QIODevice::Append | QIODevice::Unbuffered};
        s << u2;
    }
    else
    {
        out_ << u2;
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_s2(short s2, bool to_buffer)
{
    if (to_buffer)
    {
        QDataStream s{&buffer_, QIODevice::Append | QIODevice::Unbuffered};
        s << s2;
    }
    else
    {
        out_ << s2;
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_u1(
    uchar u1,
    bool to_buffer /*= false*/)
{
    if (to_buffer)
    {
        QDataStream s{&buffer_, QIODevice::Append | QIODevice::Unbuffered};
        s << u1;
    }
    else
    {
        out_ << u1;
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_string(
    const QString& s,
    bool to_buffer /*= false*/)
{
    if (to_buffer)
    {
        QDataStream stream{&buffer_, QIODevice::Append | QIODevice::Unbuffered};
        stream.writeRawData(s.toStdString().c_str(), s.size());
    }
    else
    {
        out_.writeRawData(s.toStdString().c_str(), s.size());
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_intro()
{
    write_u4(MAGIC_NUMBER_);
    write_u2(MINOR_JDK_VERSION_);
    write_u2(MAJOR_JDK_VERSION_);
}

void pascal2java::BytecodeGeneratorVisitor::write_constant_table()
{
    ConstantTable t = constants_[program_name_];
    int amount = t.size();
    write_u2(amount + 1);

    for (int i = 1; i <= amount; ++i)
    {
        ConstantTable::Type type = t.type(i);

        if (type == ConstantTable::Type::UTF8)
        {
            QString v = t.find_utf8_by_number(i);

            write_u1(UTF8_TYPE_);
            write_u2(v.size());
            write_string(v);
        }
        else if (type == ConstantTable::Type::CLASS)
        {
            int v = t.find_class_by_number(i);

            write_u1(CLASS_TYPE_);
            write_u2(v);
        }
        else if (type == ConstantTable::Type::NAME_AND_TYPE)
        {
            auto v = t.find_name_and_type_by_number(i);

            write_u1(NAME_AND_TYPE_TYPE_);
            write_u2(v.first);
            write_u2(v.second);
        }
        else if (type == ConstantTable::Type::FIELD_REF)
        {
            auto v = t.find_field_ref_by_number(i);

            write_u1(FIELD_REF_TYPE_);
            write_u2(v.first);
            write_u2(v.second);
        }
        else if (type == ConstantTable::Type::METHOD_REF)
        {
            auto v = t.find_method_ref_by_number(i);

            write_u1(METHOD_REF_TYPE_);
            write_u2(v.first);
            write_u2(v.second);
        }
        else if (type == ConstantTable::Type::STRING)
        {
            int v = t.find_string_by_number(i);

            write_u1(STRING_TYPE_);
            write_u2(v);
        }
        else if (type == ConstantTable::Type::NOT_FOUND)
        {
            QString message = QString("Constant with number %1 was not found")
                .arg(i);
            throw std::exception(message.toStdString().c_str());
        }
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_class_metadata()
{
    // Access flags.
    write_u2(ACC_SUPER | ACC_PUBLIC);

    // Current and parent class.
    write_u2(constants_[program_name_].find_class(program_name_));
    write_u2(constants_[program_name_].find_class("java/lang/Object"));

    // Number of interfaces.
    write_u2(0);
}

void pascal2java::BytecodeGeneratorVisitor::write_field_table()
{
    auto fields = constants_[program_name_].field_refs();
    write_u2(fields.size());

    foreach (auto f, fields.keys())
    {
        // Field access flags.
        write_u2(ACC_PUBLIC | ACC_STATIC);

        auto nat =
            constants_[program_name_].find_name_and_type_by_number(f.second);

        // Field name.
        write_u2(nat.first);
        // Field descriptor.
        write_u2(nat.second);

        // Number of field attributes.
        write_u2(0);
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_method_table()
{
    auto methods = constants_[program_name_].method_refs();
    write_u2(methods.size() - 4);

    write_default_constructor();
    write_main_method();

    foreach (auto m, methods.keys())
    {
        int id = methods.value(m);
        if (id == 15 || id == 9 || id == 21 || id == 25 || id == 29 || id == 33)
        {
            continue;
        }

        // Method access flags.
        write_u2(ACC_PUBLIC | ACC_STATIC);
       
        auto nat = constants_[program_name_]
            .find_name_and_type_by_number(m.second);
        // Method name.
        write_u2(nat.first);
        // Method descriptor.
        write_u2(nat.second);

        // Amount of attributes.
        write_u2(1);

        // Attribute name.
        write_u2(constants_[program_name_].find_utf8("Code"));

        QString name = constants_[program_name_].find_utf8_by_number(nat.first);
        parent_name_ = name;
        int number_of_local_vars = constants_[parent_name_].utf8().size();

        for (auto f : proc_and_func_)
        {
            if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
            {
                if (f->func_decl_->heading_->name_->name_ == name)
                {
                    f->func_decl_->body_->stmt_->accept(*this);
                    break;
                }
            }
            else if (f->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
            {
                if (f->proc_decl_->heading_->name_->name_ == name)
                {
                    f->proc_decl_->body_->stmt_->accept(*this);
                    write_u1(RETURN_, true);
                    break;
                }
            }
        }

        // Attribute value length.
        int bytecode_length = buffer_.size();
        int fields_size_without_bytecode_length = 12;
        write_u4(fields_size_without_bytecode_length + bytecode_length);

        // Stack size.
        write_u2(1000);

        // Amount of local variables (args).
        write_u2(number_of_local_vars);

        // Bytecode length.
        write_u4(bytecode_length);

        // Bytecode.
        out_.writeRawData(buffer_.data(), bytecode_length);
        buffer_.clear();

        // Number of exceptions.
        write_u2(0);

        // Number of attribute attributes.
        write_u2(0);
    }
}

void pascal2java::BytecodeGeneratorVisitor::write_default_constructor()
{
    // Method access flags.
    write_u2(ACC_PUBLIC);

    // Method name.
    int n1 = constants_[program_name_].find_utf8("<init>");
    write_u2(n1);
    // Method descriptor.
    int n2 = constants_[program_name_].find_utf8("()V");
    write_u2(n2);

    // Amount of attributes.
    write_u2(1);

    // Attribute name.
    write_u2(constants_[program_name_].find_utf8("Code"));

    // Attribute value length.
    int bytecode_length = 5;
    int fields_size_without_bytecode_length = 12;
    write_u4(fields_size_without_bytecode_length + bytecode_length);

    // Stack size.
    write_u2(10);
    // Amount of local variables (this).
    write_u2(1);

    // Bytecode length.
    write_u4(bytecode_length);

    // Bytecode.
    // Load "this" on the stack.
    write_u1(ALOAD_0_);
    // Call to object's contructor.
    write_u1(INVOKESPECIAL_);
    int nat = constants_[program_name_].find_name_and_type(n1, n2);
    write_u2(
        constants_[program_name_].find_method_ref_id_by_name_and_type(nat));
    write_u1(RETURN_);

    // Number of exceptions.
    write_u2(0);

    // Number of attribute attributes.
    write_u2(0);
}

void pascal2java::BytecodeGeneratorVisitor::write_main_method()
{
    // Method access flags.
    write_u2(ACC_PUBLIC | ACC_STATIC);

    // Method name.
    write_u2(constants_[program_name_].find_utf8("main"));
    // Method descriptor.
    write_u2(constants_[program_name_].find_utf8("([Ljava/lang/String;)V"));

    // Amount of attributes.
    write_u2(1);

    // Attribute name.
    write_u2(constants_[program_name_].find_utf8("Code"));

    parent_name_ = program_name_;

    for (auto v : main_block_->vars_)
    {
        if (TypeArray* ta = dynamic_cast<TypeArray*>(v.second))
        {
             if (SubrangeType* st =
                 dynamic_cast<SubrangeType*>(ta->index_type_list_[0]))
             {
                 int count = st->max()->get_number().get_int();
                 write_u1(SIPUSH_, true);
                 write_s2(count, true);

                 int type = -1;
                 TypeId* ti = dynamic_cast<TypeId*>(ta->type_denoter_);
                 if (ti->type_ == "integer")
                 {
                     type = 10;
                 }
                 else if (ti->type_ == "char")
                 {
                     type = 5;
                 }

                 write_u1(NEWARRAY_, true);
                 write_u1(type, true);

                write_u1(PUTSTATIC_, true);
                write_u2(constants_[program_name_]
                    .find_field_ref(v.first), true);
             }
        }
    }

    main_block_->stmt_->accept(*this);

    // Attribute value length.
    write_u1(RETURN_, true);
    int bytecode_length = buffer_.size();
    int fields_size_without_bytecode_length = 12;
    write_u4(fields_size_without_bytecode_length + bytecode_length);

    // Stack size.
    write_u2(1000);

    // Amount of local variables (args).
    write_u2(1);

    // Bytecode length.
    write_u4(bytecode_length);

    // Bytecode.
    out_.writeRawData(buffer_.data(), bytecode_length);
    buffer_.clear();

    // Number of exceptions.
    write_u2(0);

    // Number of attribute attributes.
    write_u2(0);
}

void pascal2java::BytecodeGeneratorVisitor::write_class_attributes()
{
    // Number of class attributes.
    write_u2(1);

    // Class attributes table.
    // Attribute name.
    write_u2(constants_[program_name_].find_utf8("SourceFile"));
    // Attribute value length.
    write_u4(2);
    // Attribute value.
    write_u2(constants_[program_name_].find_utf8(program_name_ + ".pas"));
}

bool pascal2java::BytecodeGeneratorVisitor::is_global() const
{
    return program_name_ == parent_name_;
}
