﻿#ifndef CONSTANTTABLE_H
#define CONSTANTTABLE_H

#include <QMap>
#include <QString>

namespace pascal2java
{
    struct ConstantTable
    {
        enum class Type
        {
            UTF8,
            CLASS,
            NAME_AND_TYPE,
            FIELD_REF,
            METHOD_REF,
            NOT_FOUND,
            STRING,
        };

        ConstantTable()
            : counter_{1}
        {
        }

        int insert_utf8(const QString& value)
        {
            if (!utf8_.contains(value))
            {
                utf8_.insert(value, counter_);
                utf8_ordered_ << value;

                return counter_++;
            }

            return -1;
        }

        int insert_string(int utf8)
        {
            if (!strings_.contains(utf8))
            {
                strings_.insert(utf8, counter_);

                return counter_++;
            }

            return -1;
        }

        int insert_or_find_utf8(const QString& value)
        {
            int number = insert_utf8(value);
        
            if (number == -1)
            {
                return utf8_.value(value, -1);
            }
            else
            {
                utf8_ordered_ << value;
            }

            return number;
        }

        int insert_class(int value)
        {
            if (!classes_.contains(value))
            {
                classes_.insert(value, counter_);

                return counter_++;
            }

            return -1;
        }

        int insert_name_and_type(int name_number, int type_number)
        {
            std::pair<int, int> value{name_number, type_number};

            if (!name_and_types_.contains(value))
            {
                name_and_types_.insert(value, counter_);

                return counter_++;
            }

            return -1;
        }

        int insert_field_ref(int class_number, int name_and_type_number)
        {
            std::pair<int, int> value{class_number, name_and_type_number};

            if (!field_refs_.contains(value))
            {
                field_refs_.insert(value, counter_);

                return counter_++;
            }

            return -1;
        }

        int insert_method_ref(int class_number, int name_and_type_number)
        {
            std::pair<int, int> value{class_number, name_and_type_number};

            if (!method_refs_.contains(value))
            {
                method_refs_.insert(value, counter_);

                return counter_++;
            }

            return -1;
        }

        int find_utf8(const QString& value) const
        {
            return utf8_.value(value, -1);
        }

        int find_string(int utf8) const
        {
            return strings_.value(utf8, -1);
        }

        int find_class(const QString& class_name) const
        {
            int utf8_number = utf8_.value(class_name, -1);

            return classes_.value(utf8_number, -1);
        }

        int find_type(int utf8_number) const
        {
            foreach (const auto& key, name_and_types_.keys())
            {
                if (key.first == utf8_number)
                {
                    return key.second;
                }
            }

            return -1;
        }

        QMap<QString, int> utf8() const
        {
            return utf8_;
        }

        QList<QString> utf8_ordered() const
        {
            return utf8_ordered_;
        }

        QMap<int, int> classes() const
        {
            return classes_;
        }

        QMap<std::pair<int, int>, int> name_and_types() const
        {
            return name_and_types_;
        }

        QMap<std::pair<int, int>, int> field_refs() const
        {
            return field_refs_;
        }

        QMap<std::pair<int, int>, int> method_refs() const
        {
            return method_refs_;
        }

        int size() const
        {
            return counter_ == 1 ? 1 : counter_ - 1;
        }

        Type type(int number) const
        {
            if (find_utf8_by_number(number) != "")
            {
                return Type::UTF8;
            }

            if (find_string_by_number(number) != -1)
            {
                return Type::STRING;
            }

            if (find_class_by_number(number) != -1)
            {
                return Type::CLASS;
            }

            auto temp = std::make_pair(-1, -1);

            if (find_name_and_type_by_number(number) != temp)
            {
                return Type::NAME_AND_TYPE;
            }

            if (field_refs_.key(number, temp) != temp)
            {
                return Type::FIELD_REF;
            }

            if (method_refs_.key(number, temp) != temp)
            {
                return Type::METHOD_REF;
            }

            return Type::NOT_FOUND;
        }

        QMap<int, int> strings() const
        {
            return strings_;
        }

        QString find_utf8_by_number(int number) const
        {
            return utf8_.key(number);
        }

        int find_string_by_number(int number) const
        {
            return strings_.key(number, -1);
        }

        int find_class_by_number(int number) const
        {
            return classes_.key(number, -1);
        }

        std::pair<int, int> find_name_and_type_by_number(int number) const
        {
            return name_and_types_.key(number, std::make_pair(-1, -1));
        }

        std::pair<int, int> find_field_ref_by_number(int number) const
        {
            return field_refs_.key(number, std::make_pair(-1, -1));
        }

        std::pair<int, int> find_method_ref_by_number(int number) const
        {
            return method_refs_.key(number, std::make_pair(-1, -1));
        }

        int find_method_ref_by_name(const QString& name) const
        {
            int utf8 = find_utf8(name);

            foreach (auto m, method_refs_.keys())
            {
                foreach (auto nat, name_and_types_.keys())
                {
                    if (name_and_types_.value(nat) == m.second)
                    {
                        if (nat.first == utf8)
                        {
                            return method_refs_.value(m);
                        }
                    }
                }
            }

            return -1;
        }

        int find_name_and_type(int name, int type) const
        {
            foreach (auto nat, name_and_types_.keys())
            {
                if (nat.first == name && nat.second == type)
                {
                    return name_and_types_.value(nat);
                }
            }

            return -1;
        }

        int find_method_ref_id_by_name_and_type(int nat) const
        {
            foreach (auto m, method_refs_.keys())
            {
                if (m.second == nat)
                {
                    return method_refs_.value(m);
                }
            }

            return -1;
        }

        int find_field_ref(const QString& name) const
        {
            int utf8 = find_utf8(name);

            foreach (auto f, field_refs_.keys())
            {
                foreach (auto nat, name_and_types_.keys())
                {
                    if (name_and_types_.value(nat) == f.second)
                    {
                        if (nat.first == utf8)
                        {
                            return field_refs_.value(f);
                        }
                    }
                }
            }

            return -1;
        }

        int local_var_index(const QString& var) const
        {
            for (int i = 0, size = utf8_ordered_.size(); i < size; ++i)
            {
                if (var == utf8_ordered_[i])
                {
                    return size - i - 1;
                }
            }

            return -1;
        }

    private:
        QMap<QString, int> utf8_;
        QList<QString> utf8_ordered_;
        QMap<int, int> strings_;
        QMap<int, int> classes_;
        QMap<std::pair<int, int>, int> name_and_types_;
        QMap<std::pair<int, int>, int> field_refs_;
        QMap<std::pair<int, int>, int> method_refs_;
        int counter_;
    };
}

#endif // CONSTANTTABLE_H
