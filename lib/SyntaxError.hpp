#ifndef ERROR_H
#define ERROR_H

#include <QString>
#include "location.hh"

namespace pascal2java
{
    /*!
     * Syntax error class.
     */
    class SyntaxError
    {
    public:
        SyntaxError(
            const pascal2java::location& location,
            const QString& message);

        /*
         * Returns error message.
         */
        QString error() const;

    private:
        pascal2java::location location_;
        QString message_;
    };
}

#endif
