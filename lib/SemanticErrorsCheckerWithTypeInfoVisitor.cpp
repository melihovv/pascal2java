﻿#include "SemanticErrorsCheckerWithTypeInfoVisitor.hpp"
#include "Ast.hpp"

pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::SemanticErrorsCheckerWithTypeInfoVisitor()
    : check_func_return_value_{std::make_tuple(false, nullptr, nullptr)},
      assigned_expr_{nullptr}
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    Program* program)
{
    program_name_ = program->name_;
    parent_name_ = program_name_;

    funcs_ = program->block_->proc_and_func_;

    program->block_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(Block* block)
{
    for (auto f : block->proc_and_func_)
    {
        f->accept(*this);
    }

    block->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    TypeId* type_id)
{
    if (std::get<0>(check_func_return_value_))
    {
        FunctionDeclaration* f = std::get<2>(check_func_return_value_);
        Expr* e = std::get<1>(check_func_return_value_);

        if (e->type_ == Expr::Type::INT)
        {
            if (type_id->type_ != "integer")
            {
                throw_exception("Invalid return type");
            }
        }
        else if (e->type_ == Expr::Type::STRING)
        {
            if (type_id->type_ != "char")
            {
                throw_exception("Invalid return type");
            }
        }
        else
        {
            throw_exception("Invalid return type");
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    TypeArray* type_array)
{
    type_array->type_denoter_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    SubrangeType* subrange_type)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    FunctionDeclaration* func_decl)
{
    QString temp = parent_name_;
    parent_name_ = func_decl->heading_->name_->name_;
    func_decl->body_->accept(*this);
    parent_name_ = temp;
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ProcedureDeclaration* proc_decl)
{
    QString temp = parent_name_;
    parent_name_ = proc_decl->heading_->name_->name_;
    proc_decl->body_->accept(*this);
    parent_name_ = temp;
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    FunctionHeading* func_heading)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ProcedureIdentification* proc_ident)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    FunctionIdentification* func_ident)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ProcedureHeading* proc_heading)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }
    else if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    FunctionDesignator* func_designator)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    VariableIdAccess* var_id_access)
{
    if (FunctionDesignator* fd =
        dynamic_cast<FunctionDesignator*>(assigned_expr_))
    {
        if (fd->id_ == "readln")
        {
            return;
        }
    }

    if (assigned_expr_ != nullptr &&
        var_id_access->type_ != assigned_expr_->type_)
    {
        throw_exception("Assign different type");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    IndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (auto e : indexed_variable->expr_list_)
    {
        if (e->type_ != Expr::Type::INT)
        {
            throw_exception("Not int type in array element index");
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
    indexed_variable->var_access_->accept(*this);

    for (auto e : indexed_variable->expr_list_)
    {
        if (e->type_ != Expr::Type::INT)
        {
            throw_exception("Not int type in array element index");
        }
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    AssignmentStatement* statement)
{
    if (!is_global_context())
    {
        Expr::Type t = Expr::Type::NOT_INIT;

        for (auto f : funcs_)
        {
            if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
            {
                if (f->func_decl_->heading_->name_->name_ == parent_name_)
                {
                    check_func_return_value_ =
                        std::make_tuple(true, statement->expr_, f->func_decl_);
                    f->func_decl_->heading_->return_type_->accept(*this);
                    check_func_return_value_ =
                        std::make_tuple(false, nullptr, nullptr);
                }
            }
        }
    }

    assigned_expr_ = statement->expr_;
    statement->var_access_->accept(*this);
    assigned_expr_ = nullptr;
    statement->expr_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ProcedureStatement* statement)
{
    for (auto f : funcs_)
    {
        if (f->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
        {
            if (f->func_decl_->heading_->name_->name_ == statement->id_)
            {
                for (auto p : f->func_decl_->heading_->params_)
                {
                    for (int i = 0, size = statement->params_.size(); i < size; ++i)
                    {
                        Type* t = nullptr;

                        if (TypeArray* ta = dynamic_cast<TypeArray*>(t))
                        {
                            t = ta->type_denoter_;
                        }
                        else
                        {
                            t = p->val_param_spec_->vars_[i].second;
                        }

                        if (TypeId* ti = dynamic_cast<TypeId*>(t))
                        {
                            if (ti->type_ == "integer")
                            {
                                if (statement->params_[i]->expr_->type_ !=
                                    Expr::Type::INT)
                                {
                                    throw_exception(
                                        "Call function with invalid argument type");
                                }
                            }
                            else if (ti->type_ == "char")
                            {
                                if (statement->params_[i]->expr_->type_ !=
                                    Expr::Type::STRING)
                                {
                                    throw_exception(
                                        "Call function with invalid argument type");
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (f->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
        {
            if (f->proc_decl_->heading_->name_->name_ == statement->id_)
            {
                for (auto p : f->proc_decl_->heading_->params_)
                {
                    for (int i = 0, size = statement->params_.size(); i < size; ++i)
                    {
                        Type* t = nullptr;

                        if (TypeArray* ta = dynamic_cast<TypeArray*>(t))
                        {
                            t = ta->type_denoter_;
                        }
                        else
                        {
                            t = p->val_param_spec_->vars_[i].second;
                        }

                        if (TypeId* ti = dynamic_cast<TypeId*>(t))
                        {
                            if (ti->type_ == "integer")
                            {
                                if (statement->params_[i]->expr_->type_ !=
                                    Expr::Type::INT)
                                {
                                    throw_exception(
                                        "Call procedure with invalid argument type");
                                }
                            }
                            else if (ti->type_ == "char")
                            {
                                if (statement->params_[i]->expr_->type_ !=
                                    Expr::Type::STRING)
                                {
                                    throw_exception(
                                        "Call procedure with invalid argument type");
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    for (ActualParameter* p : statement->params_)
    {
        p->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    CaseStatement* statement)
{
    if (statement->expr_->type_ != Expr::Type::INT)
    {
        throw_exception("Not int type in case statement");
    }

    statement->expr_->accept(*this);

    for (CaseListElement* e : statement->elems_)
    {
        e->accept(*this);
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    CaseListElement* case_list_elem)
{
    for (Constant* c : case_list_elem->constants_)
    {
        if (c->const_type() != Constant::Type::NUMBER)
        {
            throw_exception("Not int type in case element");
        }

        c->accept(*this);
    }

    case_list_elem->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    RepeatStatement* statement)
{
    if (statement->expr_->type_ != Expr::Type::BOOL)
    {
        throw_exception("Not bool expression in while loop");
    }

    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    IfWithoutElseStatement* statement)
{
    if (statement->condition_->type_ != Expr::Type::BOOL)
    {
        throw_exception("Not bool expression in if statement");
    }

    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    IfElseStatement* statement)
{
    if (statement->condition_->type_ != Expr::Type::BOOL)
    {
        throw_exception("Not bool expression in if statement");
    }

    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
    statement->else_stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    WhileStatement* statement)
{
    if (statement->expr_->type_ != Expr::Type::BOOL)
    {
        throw_exception("Not bool expression in if statement");
    }

    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ForToStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ForDowntoStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    EqualExpr* equal_expr)
{
    if (equal_expr->left_->type_ != equal_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    NotEqualExpr* not_equal_expr)
{
    if (not_equal_expr->left_->type_ != not_equal_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    LtExpr* lt_expr)
{
    if (lt_expr->left_->type_ != lt_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    GtExpr* gt_expr)
{
    if (gt_expr->left_->type_ != gt_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    LeExpr* le_expr)
{
    if (le_expr->left_->type_ != le_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    GeExpr* ge_expr)
{
    if (ge_expr->left_->type_ != ge_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    AndExpr* and_expr)
{
    if (and_expr->left_->type_ != and_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    OrExpr* or_expr)
{
    if (or_expr->left_->type_ != or_expr->right_->type_)
    {
        throw_exception("Comparing of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    NotExpr* not_expr)
{
    if (not_expr->expr_->type_ != Expr::Type::BOOL)
    {
        throw_exception("Negation of not bool");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    PlusExpr* plus_expr)
{
    if (plus_expr->left_->type_ != plus_expr->right_->type_)
    {
        throw_exception("Adding of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    MinusExpr* minus_expr)
{
    if (minus_expr->left_->type_ != minus_expr->right_->type_)
    {
        throw_exception("Subtraction of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    StarExpr* star_expr)
{
    if (star_expr->left_->type_ != star_expr->right_->type_)
    {
        throw_exception("Multiplication of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    SlashExpr* slash_expr)
{
    if (slash_expr->left_->type_ != slash_expr->right_->type_)
    {
        throw_exception("Division of different types");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    DivExpr* div_expr)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    ModExpr* mod_expr)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    UnaryMinus* minus)
{
    if (minus->expr_->type_ != Expr::Type::INT)
    {
        throw_exception("Unary minus with not int");
    }
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::visit(
    Constant* constant)
{
}

void pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::throw_exception(
    const QString& message)
{
    throw std::exception(message.toStdString().c_str());
}

bool
pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor::is_global_context()
const
{
    return parent_name_ == program_name_;
}
