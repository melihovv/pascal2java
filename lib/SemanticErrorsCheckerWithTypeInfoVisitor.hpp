﻿#ifndef SEMANTICERRORSCHECKERWITHTYPEINFOVISITOR_H
#define SEMANTICERRORSCHECKERWITHTYPEINFOVISITOR_H

#include "IVisitor.hpp"
#include <QtCore/QString>
#include <QtCore/QList>
#include <tuple>

namespace pascal2java
{
    class SemanticErrorsCheckerWithTypeInfoVisitor : public IVisitor
    {
    public:
        SemanticErrorsCheckerWithTypeInfoVisitor();

        virtual void visit(Program* program) override;
        virtual void visit(Block* block) override;
        virtual void visit(TypeId* type_id) override;
        virtual void visit(TypeArray* type_array) override;
        virtual void visit(SubrangeType* subrange_type) override;
        virtual void visit(FunctionDeclaration* func_decl) override;
        virtual void visit(ProcedureDeclaration* proc_decl) override;
        virtual void visit(ValueParameterSpecification* val_par_spec) override;
        virtual void visit(VariableParameterSpecification* var_par_spec)
            override;
        virtual void visit(FunctionHeading* func_heading) override;
        virtual void visit(FormalParameterSection* formal_param_sec) override;
        virtual void visit(ProcedureIdentification* proc_ident) override;
        virtual void visit(FunctionIdentification* func_ident) override;
        virtual void visit(ProcedureHeading* proc_heading) override;
        virtual void visit(ProcOrFuncDeclaration* proc_or_func_decl) override;
        virtual void visit(ActualParameter* actual_parameter) override;
        virtual void visit(FunctionDesignator* func_designator) override;
        virtual void visit(VariableIdAccess* var_id_access) override;
        virtual void visit(IndexedVariable* indexed_variable) override;
        virtual void visit(WriteToIndexedVariable* indexed_variable) override;
        virtual void visit(AssignmentStatement* statement) override;
        virtual void visit(ProcedureStatement* statement) override;
        virtual void visit(CompoundStatement* statement) override;
        virtual void visit(CaseStatement* statement) override;
        virtual void visit(CaseListElement* case_list_elem) override;
        virtual void visit(RepeatStatement* statement) override;
        virtual void visit(IfWithoutElseStatement* statement) override;
        virtual void visit(IfElseStatement* statement) override;
        virtual void visit(WhileStatement* statement) override;
        virtual void visit(ForToStatement* statement) override;
        virtual void visit(ForDowntoStatement* statement) override;
        virtual void visit(EqualExpr* equal_expr) override;
        virtual void visit(NotEqualExpr* not_equal_expr) override;
        virtual void visit(LtExpr* lt_expr) override;
        virtual void visit(GtExpr* gt_expr) override;
        virtual void visit(LeExpr* le_expr) override;
        virtual void visit(GeExpr* ge_expr) override;
        virtual void visit(AndExpr* and_expr) override;
        virtual void visit(OrExpr* or_expr) override;
        virtual void visit(NotExpr* not_expr) override;
        virtual void visit(PlusExpr* plus_expr) override;
        virtual void visit(MinusExpr* minus_expr) override;
        virtual void visit(StarExpr* star_expr) override;
        virtual void visit(SlashExpr* slash_expr) override;
        virtual void visit(DivExpr* div_expr) override;
        virtual void visit(ModExpr* mod_expr) override;
        virtual void visit(UnaryMinus* minus) override;
        virtual void visit(Constant* constant) override;

    private:
        static void throw_exception(const QString& message);
        bool is_global_context() const;

        QString parent_name_;
        QString program_name_;
        QList<ProcOrFuncDeclaration*> funcs_;
        std::tuple<bool, Expr*, FunctionDeclaration*> check_func_return_value_;
        Expr* assigned_expr_;
    };
}

#endif // SEMANTICERRORSCHECKERWITHTYPEINFOVISITOR_H
