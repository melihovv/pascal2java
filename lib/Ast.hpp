﻿#ifndef AST_H
#define AST_H

#include <QString>
#include <QStringList>
#include <QList>
#include "Number.hpp"
#include <QMap>
#include "IVisitor.hpp"

namespace pascal2java
{
    struct Node
    {
        virtual ~Node()
        {
        }

        virtual void accept(IVisitor& visitor) = 0;
    };

    struct Expr : public Node
    {
        enum class Type
        {
            INT,
            STRING,
            BOOL,
            VOID,
            NOT_INIT,
        };

        Expr()
            : type_{Type::NOT_INIT}
        {
        }

        QString type() const
        {
            QString result;

            if (type_ == Type::INT)
            {
                result = "INT";
            }
            else if (type_ == Type::STRING)
            {
                result = "STRING";
            }
            else if (type_ == Type::BOOL)
            {
                result = "BOOL";
            }
            else if (type_ == Type::VOID)
            {
                result = "VOID";
            }
            else if (type_ == Type::NOT_INIT)
            {
                result = "NOT_INIT";
            }

            return result;
        }

        Type type_;
    };

    struct UnaryExpr : public Expr
    {
        UnaryExpr()
            : expr_{nullptr}
        {
        }

        explicit UnaryExpr(Expr* expr)
            : expr_{expr}
        {
        }

        Expr* expr_;
    };

    struct BinaryExpr : public Expr
    {
        BinaryExpr()
            : left_{nullptr},
              right_{nullptr}
        {
        }

        BinaryExpr(Expr* left, Expr* right)
            : left_{left},
              right_{right}
        {
        }

        Expr* left_;
        Expr* right_;
    };

    struct EqualExpr : public BinaryExpr
    {
        EqualExpr()
        {
        }

        EqualExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct NotEqualExpr : public BinaryExpr
    {
        NotEqualExpr()
        {
        }

        NotEqualExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct LtExpr : public BinaryExpr
    {
        LtExpr()
        {
        }

        LtExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct GtExpr : public BinaryExpr
    {
        GtExpr()
        {
        }

        GtExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct LeExpr : public BinaryExpr
    {
        LeExpr()
        {
        }

        LeExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct GeExpr : public BinaryExpr
    {
        GeExpr()
        {
        }

        GeExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct PlusExpr : public BinaryExpr
    {
        PlusExpr()
        {
        }

        PlusExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct MinusExpr : public BinaryExpr
    {
        MinusExpr()
        {
        }

        MinusExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct OrExpr : public BinaryExpr
    {
        OrExpr()
        {
        }

        OrExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct StarExpr : public BinaryExpr
    {
        StarExpr()
        {
        }

        StarExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct SlashExpr : public BinaryExpr
    {
        SlashExpr()
        {
        }

        SlashExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct DivExpr : public BinaryExpr
    {
        DivExpr()
        {
        }

        DivExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct ModExpr : public BinaryExpr
    {
        ModExpr()
        {
        }

        ModExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct AndExpr : public BinaryExpr
    {
        AndExpr()
        {
        }

        AndExpr(Expr* left, Expr* right)
            : BinaryExpr{left, right}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct UnaryMinus : public UnaryExpr
    {
        UnaryMinus()
        {
        }

        explicit UnaryMinus(Expr* expr)
            : UnaryExpr(expr)
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct NotExpr : public UnaryExpr
    {
        NotExpr()
        {
        }

        explicit NotExpr(Expr* child)
            : UnaryExpr{child}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct Sign
    {
        enum class Type
        {
            MINUS,
            PLUS,
            NOT_INIT,
        };

        Sign()
            : type_{Type::NOT_INIT}
        {
        }

        explicit Sign(const Type& type)
            : type_{type}
        {
        }

        bool isMinus() const
        {
            return type_ == Type::MINUS;
        }

        bool isPlus() const
        {
            return type_ == Type::PLUS;
        }

        Type type() const
        {
            return type_;
        }

    private:
        Type type_;
    };

    struct Constant : public Expr
    {
        enum class Type
        {
            NUMBER,
            ID,
            STRING,
            NOT_INIT,
        };

        Constant()
        {
            const_type_ = Type::NOT_INIT;
        }

        Constant(const Number& number)
            : const_type_{Type::NUMBER},
              value_{number}
        {
        }

        Constant(const QString& value, const Type& type)
            : const_type_{type},
              value_{value}
        {
        }

        Number get_number() const
        {
            return boost::get<Number>(value_);
        }

        QString get_string_or_id() const
        {
            return boost::get<QString>(value_);
        }

        Type const_type() const
        {
            return const_type_;
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

    private:
        Type const_type_;
        boost::variant<Number, QString> value_;
    };

    struct Type : public Node
    {
        virtual Type* clone() = 0;
    };

    struct OrdinalType : public Type
    {
    };

    struct SubrangeType : public OrdinalType
    {
        SubrangeType()
            : min_{nullptr},
              max_{nullptr}
        {
        }

        SubrangeType(Constant* min, Constant* max)
            : min_{min},
              max_{max}
        {
        }

        Constant* min() const
        {
            return min_;
        }

        Constant* max() const
        {
            return max_;
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Type* clone() override
        {
            return new SubrangeType(min_, max_);
        }

    private:
        Constant* min_;
        Constant* max_;
    };

    struct TypeId : public Type
    {
        explicit TypeId(QString& type)
            : type_{type}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Type* clone() override
        {
            return new TypeId(type_);
        }

        QString type_;
    };

    struct TypeArray : public Type
    {
        TypeArray()
            : is_packed_{false},
              type_denoter_{nullptr}
        {
        }

        TypeArray(
            Type* type_denoter,
            QList<OrdinalType*>& index_type_list,
            bool is_packed = false)
            : is_packed_{is_packed},
              type_denoter_{type_denoter},
              index_type_list_{index_type_list}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Type* clone() override
        {
            return new TypeArray(type_denoter_, index_type_list_, is_packed_);
        }

        bool is_packed_;
        Type* type_denoter_;
        QList<OrdinalType*> index_type_list_;
    };

    struct ProcOrFuncDeclaration : public Node
    {
        enum class Type
        {
            PROCEDURE,
            FUNCTION
        };

        ProcOrFuncDeclaration()
            : proc_decl_{nullptr},
              func_decl_{nullptr}
        {
        }

        ProcOrFuncDeclaration(const Type& type,
                              ProcedureDeclaration* proc_decl,
                              FunctionDeclaration* func_decl)
            : type_{type},
              proc_decl_{proc_decl},
              func_decl_{func_decl}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Type type_;
        ProcedureDeclaration* proc_decl_;
        FunctionDeclaration* func_decl_;
    };

    struct ProcedureDeclaration : public Node
    {
        ProcedureDeclaration()
            : heading_{nullptr},
              body_{nullptr},
              is_forward_{true}
        {
        }

        ProcedureDeclaration(ProcedureHeading* heading,
                             Block* body = nullptr,
                             bool is_forward = true)
            : heading_{heading},
              body_{body},
              is_forward_{is_forward}

        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        ProcedureHeading* heading_;
        Block* body_;
        bool is_forward_;
    };

    struct ProcedureIdentification : public Node
    {
        ProcedureIdentification()
        {
        }

        ProcedureIdentification(QString name)
            : name_{name}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString name_;
    };

    struct ParameterSpecification : public Node
    {
        ParameterSpecification()
        {
        }

        ParameterSpecification(QList<std::pair<QString, Type*>> vars)
            : vars_{vars}
        {
        }

        QList<std::pair<QString, Type*>> vars_;
    };

    struct ValueParameterSpecification : public ParameterSpecification
    {
        ValueParameterSpecification(QList<std::pair<QString, Type*>> vars)
            : ParameterSpecification(vars)
        {
        };

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct VariableParameterSpecification : public ParameterSpecification
    {
        VariableParameterSpecification(QList<std::pair<QString, Type*>> vars)
            : ParameterSpecification(vars)
        {
        };

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct FormalParameterSection : public Node
    {
        enum class Type
        {
            VALUE,
            VARIABLE
        };

        FormalParameterSection()
            : val_param_spec_{nullptr},
              var_param_spec_{nullptr}
        {
        }

        FormalParameterSection(const Type& type,
                               ValueParameterSpecification* val_param_spec,
                               VariableParameterSpecification* var_param_spec)
            : type_{type},
              val_param_spec_{val_param_spec},
              var_param_spec_{var_param_spec}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Type type() const
        {
            return type_;
        }

        Type type_;
        ValueParameterSpecification* val_param_spec_;
        VariableParameterSpecification* var_param_spec_;
    };

    struct ProcedureHeading : public Node
    {
        ProcedureHeading()
            : name_{nullptr}
        {
        }

        ProcedureHeading(ProcedureIdentification* name,
                         QList<FormalParameterSection*> params = QList<FormalParameterSection*>())
            : name_{name},
              params_{params}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        int params_amount() const
        {
            int count = 0;

            for (auto p : params_)
            {
                count += p->val_param_spec_->vars_.size();
            }

            return count;
        }

        ProcedureIdentification* name_;
        QList<FormalParameterSection*> params_;
    };

    struct FunctionDeclaration : public Node
    {
        FunctionDeclaration()
            : heading_{nullptr},
              body_{nullptr}
        {
        }

        FunctionDeclaration(FunctionHeading* heading,
                            Block* body = nullptr,
                            bool is_forward = true)
            : heading_{heading},
              body_{body},
              is_forward_{is_forward}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        FunctionHeading* heading_;
        Block* body_;
        bool is_forward_;
    };

    struct FunctionIdentification : public Node
    {
        FunctionIdentification()
        {
        }

        FunctionIdentification(QString& name)
            : name_{name}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString name_;
    };

    struct FunctionHeading : public Node
    {
        FunctionHeading()
            : name_{nullptr},
              return_type_{nullptr}
        {
        }

        FunctionHeading(FunctionIdentification* name,
                        Type* return_type,
                        QList<FormalParameterSection*> params = QList<FormalParameterSection*>())
            : name_{name},
              return_type_{return_type},
              params_{params}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        int params_amount() const
        {
            int count = 0;

            for (auto p : params_)
            {
                count += p->val_param_spec_->vars_.size();
            }

            return count;
        }

        FunctionIdentification* name_;
        Type* return_type_;
        QList<FormalParameterSection*> params_;
    };

    struct VariableAccess : public Expr
    {
    };

    struct VariableIdAccess : public VariableAccess
    {
        VariableIdAccess()
        {
        }

        explicit VariableIdAccess(QString& id)
            : id_{id}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString id_;
    };

    struct IndexedVariable : public VariableAccess
    {
        IndexedVariable()
            : var_access_{nullptr}
        {
        }

        IndexedVariable(
            VariableAccess* var_access,
            QList<Expr*>& expr_list)
            : var_access_{var_access},
              expr_list_{expr_list}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        VariableAccess* var_access_;
        QList<Expr*> expr_list_;
    };

    struct WriteToIndexedVariable : public VariableAccess
    {
        WriteToIndexedVariable()
            : var_access_{nullptr}
        {
        }

        WriteToIndexedVariable(
            VariableAccess* var_access,
            QList<Expr*>& expr_list)
            : var_access_{var_access},
              expr_list_{expr_list}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        VariableAccess* var_access_;
        QList<Expr*> expr_list_;
    };

    struct Statement : public Node
    {
    };

    struct AssignmentStatement : public Statement
    {
        AssignmentStatement()
            : var_access_{nullptr},
              expr_{nullptr}
        {
        }

        AssignmentStatement(VariableAccess* var_access, Expr* expr)
            : var_access_{var_access},
              expr_{expr}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        VariableAccess* var_access_;
        Expr* expr_;
    };

    struct ActualParameter : public Node
    {
        ActualParameter()
            : expr_{nullptr}
        {
        }

        explicit ActualParameter(Expr* expr)
            : expr_{expr}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Expr* expr_;
    };

    struct ProcedureStatement : public Statement
    {
        ProcedureStatement()
        {
        }

        ProcedureStatement(
            QString& id,
            QList<ActualParameter*>& actual_parameters)
            : id_{id},
              params_{actual_parameters}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString id_;
        QList<ActualParameter*> params_;
    };

    struct CompoundStatement : public Statement
    {
        CompoundStatement()
        {
        }

        void append(Statement* statement)
        {
            stmts_.append(statement);
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QList<Statement*> stmts_;
    };

    struct CaseListElement : public Node
    {
        CaseListElement()
            : stmt_{nullptr}
        {
        }

        CaseListElement(
            QList<Constant*>& constants,
            Statement* stmt)
            : constants_{constants},
              stmt_{stmt}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QList<Constant*> constants_;
        Statement* stmt_;
    };

    struct CaseStatement : public Statement
    {
        CaseStatement()
            : expr_{nullptr}
        {
        }

        CaseStatement(
            Expr* expr,
            QList<CaseListElement*>& elements)
            : expr_{expr},
              elems_{elements}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Expr* expr_;
        QList<CaseListElement*> elems_;
    };

    struct RepeatStatement : public Statement
    {
        RepeatStatement()
            : stmt_{nullptr},
              expr_{nullptr}
        {
        }

        RepeatStatement(CompoundStatement* statement, Expr* expr)
            : stmt_{statement},
              expr_{expr}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        CompoundStatement* stmt_;
        Expr* expr_;
    };

    struct IfStatement : public Statement
    {
        IfStatement()
            : condition_{nullptr},
              stmt_{nullptr},
              else_stmt_{nullptr}
        {
        }

        IfStatement(
            Expr* condition,
            Statement* stmt,
            Statement* else_stmt)
            : condition_{condition},
              stmt_{stmt},
              else_stmt_{else_stmt}
        {
        }

        Expr* condition_;
        Statement* stmt_;
        Statement* else_stmt_;
    };

    struct IfWithoutElseStatement : public IfStatement
    {
        IfWithoutElseStatement()
        {
        }

        IfWithoutElseStatement(Expr* condition, Statement* stmt)
            : IfStatement(condition, stmt, nullptr)
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct IfElseStatement : public IfStatement
    {
        IfElseStatement()
        {
        }

        IfElseStatement(
            Expr* condition,
            Statement* stmt,
            Statement* else_stmt)
            : IfStatement(condition, stmt, else_stmt)
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct WhileStatement : public Statement
    {
        WhileStatement()
            : stmt_{nullptr},
              expr_{nullptr}
        {
        }

        WhileStatement(Statement* statement, Expr* expr)
            : stmt_{statement},
              expr_{expr}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        Statement* stmt_;
        Expr* expr_;
    };

    struct ForStatement : public Statement
    {
        ForStatement()
            : from_{nullptr},
              to_{nullptr},
              stmt_{nullptr}
        {
        }

        ForStatement(
            QString& id,
            Expr* from,
            Expr* to,
            Statement* stmt)
            : id_{id},
              from_{from},
              to_{to},
              stmt_{stmt}
        {
        }

        QString id_;
        Expr* from_;
        Expr* to_;
        Statement* stmt_;
    };

    struct ForToStatement : public ForStatement
    {
        ForToStatement()
        {
        }

        ForToStatement(
            QString& id,
            Expr* from,
            Expr* to,
            Statement* stmt)
            : ForStatement(id, from, to, stmt)
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct ForDowntoStatement : public ForStatement
    {
        ForDowntoStatement()
        {
        }

        ForDowntoStatement(
            QString& id,
            Expr* from,
            Expr* to,
            Statement* stmt)
            : ForStatement(id, from, to, stmt)
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }
    };

    struct FunctionDesignator : public Expr
    {
        FunctionDesignator()
        {
        }

        FunctionDesignator(
            QString& id,
            QList<ActualParameter*>& actual_parameters)
            : id_{id},
              params_{actual_parameters}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString id_;
        QList<ActualParameter*> params_;
    };

    struct Block : public Node
    {
        Block()
            : stmt_{nullptr}
        {
        }

        Block(QList<long long>& labels,
              QList<std::pair<QString, Expr*>> constants,
              QMap<QString, Type*> types,
              QList<std::pair<QString, Type*>> vars,
              QList<ProcOrFuncDeclaration*> proc_and_func,
              CompoundStatement* statement)
            : labels_{labels},
              constants_{constants},
              types_{types},
              vars_{vars},
              proc_and_func_{proc_and_func},
              stmt_{statement}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QList<long long> labels_;
        QList<std::pair<QString, Expr*>> constants_;
        QMap<QString, Type*> types_;
        QList<std::pair<QString, Type*>> vars_;
        QList<ProcOrFuncDeclaration*> proc_and_func_;
        CompoundStatement* stmt_;
    };

    struct Program : public Node
    {
        Program()
            : block_{nullptr}
        {
        }

        Program(QString& name, QStringList& params,
                Block* block)
            : name_{name},
              params_{params},
              block_{block}
        {
        }

        virtual void accept(IVisitor& visitor) override
        {
            visitor.visit(this);
        }

        QString name_;
        QStringList params_;
        Block* block_;
    };
}

#endif // AST_H
