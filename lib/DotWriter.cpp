﻿#include "DotWriter.hpp"
#include <QtCore/QProcess>
#include <fstream>

void pascal2java::DotWriter::write(const QString& file_name, const QString& dot)
{
    std::ofstream dot_output;
    dot_output.open(file_name.toStdString());

    if (!dot_output.is_open())
    {
        QString message = "Couldn`t open file " + file_name;
        throw std::exception(message.toStdString().c_str());
    }

    dot_output << dot.toStdString();
    dot_output.close();

    QString temp = file_name;
    QProcess proc;
    proc.execute(
        "dot",
        {
            "-Tpng",
            file_name,
            "-o",
            temp.replace(".dot", ".png")
        }
    );
}
