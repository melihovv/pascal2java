#include "Driver.hpp"
#include "Lexer.hpp"
#include <QTextStream>

pascal2java::Driver::Driver(std::istream* is /*= nullptr*/)
    : file_name_{""}
{
    lexer_ = std::make_unique<Lexer>(is);
    parser_ = std::make_unique<Parser>(*lexer_.get(), *this);
}

pascal2java::Driver::~Driver()
{
}

void pascal2java::Driver::parse()
{
    errors_.clear();
    parser_->parse();

    if (errors_.size() != 0)
    {
        for (auto error : errors_)
        {
            QTextStream out{stdout};
            out << error.error() << endl;
        }
        return;
    }
}

void pascal2java::Driver::switch_input_stream(std::istream* is) const
{
    lexer_->switch_streams(is);
}

void pascal2java::Driver::set_file_name(const std::string& file_name)
{
    file_name_ = file_name;
}

QList<pascal2java::SyntaxError> pascal2java::Driver::errors() const
{
    return errors_;
}

pascal2java::Program* pascal2java::Driver::root() const
{
    return program_;
}

QMap<QString, pascal2java::ConstantTable>&
pascal2java::Driver::constants()
{
    return constants_;
}

void pascal2java::Driver::add_error(
    const location& location,
    const QString& message)
{
    errors_.push_back(SyntaxError(location, message));
}
