﻿#include "ConstantTablesWriter.hpp"
#include <QtCore/QTextStream>
#include <QtCore/qfile.h>

void pascal2java::ConstantTablesWriter::write(
    const QString& file_name,
    const QMap<QString, ConstantTable>& tables,
    const QString& program_name)
{
    QFile output_file{file_name};

    if (!output_file.open(QIODevice::WriteOnly))
    {
        QString message = "Couldn`t open file " + file_name;
        throw std::exception(message.toStdString().c_str());
    }

    QTextStream out{&output_file};
    QString output;

    foreach (const QString& key, tables.keys())
    {
        output += key + " table\n";
        output += "*********************************\n";
        output += format_utf8_table(tables.value(key).utf8()) + "\n";
        output += format_string_table(tables.value(key).strings()) + "\n";

        if (program_name == key)
        {
            output += format_classes_table(tables.value(key).classes()) + "\n";
            output += format_table(
                tables.value(key).name_and_types(),
                "Name and types table") + "\n";
            output += format_table(
                tables.value(key).field_refs(),
                "Field refs table") + "\n";
            output += format_table(
                tables.value(key).method_refs(),
                "Method refs table") + "\n";
        }
    }

    out << output;
}

QString pascal2java::ConstantTablesWriter::format_utf8_table(
    const QMap<QString, int>& table)
{
    QString result;
    result += "UTF-8 table\n";
    result += "---------------------------------\n";
    result += "ID\tVALUE\n";

    QList<int> ids = table.values();
    qSort(ids);

    foreach(int id, ids)
    {
        result += QString::number(id) + "\t" + table.key(id) + "\n";
    }

    return result;
}

QString pascal2java::ConstantTablesWriter::format_string_table(const QMap<int, int>& table)
{
    QString result;
    result += "String table\n";
    result += "---------------------------------\n";
    result += "ID\tVALUE\n";

    QList<int> ids = table.values();
    qSort(ids);

    foreach(int id, ids)
    {
        result += QString::number(id) + "\t" +
            QString::number(table.key(id)) + "\n";
    }

    return result;
}

QString pascal2java::ConstantTablesWriter::format_classes_table(
    const QMap<int, int>& table)
{
    QString result;
    result += "Classes table\n";
    result += "---------------------------------\n";
    result += "ID\tVALUE\n";

    QList<int> ids = table.values();
    qSort(ids);

    foreach(int id, ids)
    {
        result += QString::number(id) + "\t"
            + QString::number(table.key(id)) + "\n";
    }

    return result;
}

QString pascal2java::ConstantTablesWriter::format_table(
    const QMap<std::pair<int, int>, int>& table,
    const QString& title)
{
    QString result;
    result += title + "\n";
    result += "---------------------------------\n";
    result += "ID\tVALUE\n";

    QList<int> ids = table.values();
    qSort(ids);

    foreach(int id, ids)
    {
        result += QString::number(id) + "\t"
            + QString::number(table.key(id).first) + ","
            + QString::number(table.key(id).second) + "\n";
    }

    return result;
}
