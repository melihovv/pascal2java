#include "TreeTransformerVisitor.hpp"
#include "../lib/Ast.hpp"

void pascal2java::TreeTransformerVisitor::visit(Program* program)
{
    visit(program->block_);
}

void pascal2java::TreeTransformerVisitor::visit(Block* block)
{
    block->stmt_->accept(*this);
}

void pascal2java::TreeTransformerVisitor::visit(TypeId* type_id)
{
}

void pascal2java::TreeTransformerVisitor::visit(TypeArray* type_array)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    SubrangeType* subrange_type)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    FunctionDeclaration* func_decl)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ProcedureDeclaration* proc_decl)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    FunctionHeading* func_heading)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ProcedureIdentification* proc_ident)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    FunctionIdentification* func_ident)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ProcedureHeading* proc_heading)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ActualParameter* actual_parameter)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    FunctionDesignator* func_designator)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    VariableIdAccess* var_id_access)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    IndexedVariable* indexed_variable)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    AssignmentStatement* statement)
{
    if (IndexedVariable* v =
        dynamic_cast<IndexedVariable*>(statement->var_access_))
    {
        WriteToIndexedVariable* newNode =
            new WriteToIndexedVariable(v->var_access_, v->expr_list_);
        delete statement->var_access_;

        statement->var_access_ = newNode;
    }
}

void pascal2java::TreeTransformerVisitor::visit(
    ProcedureStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::TreeTransformerVisitor::visit(CaseStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    CaseListElement* case_list_elem)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    RepeatStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    IfWithoutElseStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    IfElseStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    WhileStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(ForToStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    ForDowntoStatement* statement)
{
}

void pascal2java::TreeTransformerVisitor::visit(EqualExpr* equal_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(
    NotEqualExpr* not_equal_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(LtExpr* lt_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(GtExpr* gt_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(LeExpr* le_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(GeExpr* ge_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(AndExpr* and_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(OrExpr* or_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(NotExpr* not_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(PlusExpr* plus_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(MinusExpr* minus_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(StarExpr* star_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(SlashExpr* slash_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(DivExpr* div_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(ModExpr* mod_expr)
{
}

void pascal2java::TreeTransformerVisitor::visit(UnaryMinus* minus)
{
}

void pascal2java::TreeTransformerVisitor::visit(Constant* constant)
{
}
