﻿#ifndef CONSTANTTABLESWRITER_H
#define CONSTANTTABLESWRITER_H

#include "ConstantTable.hpp"

namespace pascal2java
{
    class ConstantTablesWriter
    {
    public:
        static void write(
            const QString& file_name,
            const QMap<QString, ConstantTable>& tables,
            const QString& program_name);

    private:
        static QString format_utf8_table(const QMap<QString, int>& table);
        static QString format_string_table(const QMap<int, int>& table);
        static QString format_classes_table(const QMap<int, int>& table);
        static QString format_table(
            const QMap<std::pair<int, int>, int>& table,
            const QString& title);
    };
}

#endif // CONSTANTTABLESWRITER_H
