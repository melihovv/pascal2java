﻿#include "TableFillerVisitor.hpp"
#include "Ast.hpp"
#include <QtCore/QMap>

pascal2java::TableFillerVisitor::TableFillerVisitor(
    QMap<QString, ConstantTable>& constants)
    : constants_{constants},
      parent_name_{""},
      constant_number_{-1},
      var_number_{-1},
      is_constant_{false}
{
}

void pascal2java::TableFillerVisitor::visit(Program* program)
{
    program_name_ = program->name_;

    constants_[program->name_].insert_utf8("Code");

    constants_[program->name_].insert_utf8("SourceFile");
    constants_[program->name_].insert_utf8(program->name_ + ".pas");

    int number = constants_[program->name_].insert_utf8("java/lang/Object");
    int class_number = constants_[program->name_].insert_class(number);

    int n1 = constants_[program->name_].insert_utf8("<init>");
    int n2 = constants_[program->name_].insert_utf8("()V");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    number = constants_[program->name_].insert_utf8(program->name_);
    class_number = constants_[program->name_].insert_class(number);

    n1 = constants_[program->name_].insert_utf8("main");
    n2 = constants_[program->name_].insert_utf8("([Ljava/lang/String;)V");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    int utf8_number =
        constants_[program->name_].insert_utf8("RTL");
    class_number = constants_[program->name_].insert_class(utf8_number);

    n1 = constants_[program->name_].insert_utf8("printString");
    n2 = constants_[program->name_].insert_utf8("(Ljava/lang/String;)V");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    n1 = constants_[program->name_].insert_utf8("printInt");
    n2 = constants_[program->name_].insert_utf8("(I)V");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    n1 = constants_[program->name_].insert_utf8("scanString");
    n2 = constants_[program->name_].insert_utf8("()Ljava/lang/String;");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    n1 = constants_[program->name_].insert_utf8("scanInt");
    n2 = constants_[program->name_].insert_utf8("()I");
    number = constants_[program->name_].insert_name_and_type(n1, n2);
    constants_[program->name_].insert_method_ref(class_number, number);

    parent_name_ = program->name_;

    program->block_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(Block* block)
{
    if (block->constants_.size() > 0)
    {
        is_constant_ = true;

        for (const std::pair<QString, Expr*>& constant : block->constants_)
        {
            constant_number_ = constants_[parent_name_]
                .insert_utf8(constant.first);
            constant.second->accept(*this);
        }

        is_constant_ = false;
    }

    if (block->vars_.size() > 0)
    {
        for (const auto& var : block->vars_)
        {
            var_number_ = constants_[parent_name_].insert_utf8(var.first);
            var.second->accept(*this);
        }
    }

    if (block->proc_and_func_.size() > 0)
    {
        for (auto f : block->proc_and_func_)
        {
            f->accept(*this);
        }
    }

    block->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(TypeId* type_id)
{
    if (is_global())
    {
        int type_number = -1;

        if (type_id->type_ == "integer")
        {
            type_number = constants_[parent_name_].insert_or_find_utf8("I");
        }
        else if (type_id->type_ == "char")
        {
            type_number = constants_[parent_name_]
                .insert_or_find_utf8("Ljava/lang/String;");
        }

        int n = constants_[parent_name_]
            .insert_name_and_type(var_number_, type_number);
        int class_number = constants_[parent_name_].find_class(parent_name_);
        constants_[parent_name_].insert_field_ref(class_number, n);
    }
}

void pascal2java::TableFillerVisitor::visit(TypeArray* type_array)
{
    TypeId* type_id = dynamic_cast<TypeId*>(type_array->type_denoter_);
    int type_number = -1;

    if (type_id->type_ == "integer")
    {
        type_number = constants_[parent_name_].insert_or_find_utf8("[I");
    }
    else if (type_id->type_ == "char")
    {
        type_number = constants_[parent_name_]
            .insert_or_find_utf8("[Ljava/lang/String;");
    }

    int n = constants_[parent_name_]
        .insert_name_and_type(var_number_, type_number);
    int class_number = constants_[parent_name_].find_class(parent_name_);
    constants_[parent_name_].insert_field_ref(class_number, n);
}

void pascal2java::TableFillerVisitor::visit(SubrangeType* subrange_type)
{
}

void pascal2java::TableFillerVisitor::visit(FunctionDeclaration* func_decl)
{
    func_decl->heading_->accept(*this);

    QString temp = parent_name_;
    parent_name_ = func_decl->heading_->name_->name_;
    constants_.insert(parent_name_, ConstantTable{});

    for (auto p : func_decl->heading_->params_)
    {
        for (auto v : p->val_param_spec_->vars_)
        {
            constants_[parent_name_].insert_utf8(v.first);
        }
    }

    func_decl->body_->accept(*this);
    
    parent_name_ = temp;
}

void pascal2java::TableFillerVisitor::visit(ProcedureDeclaration* proc_decl)
{
    proc_decl->heading_->accept(*this);

    QString temp = parent_name_;
    parent_name_ = proc_decl->heading_->name_->name_;
    constants_.insert(parent_name_, ConstantTable{});

    for (auto p : proc_decl->heading_->params_)
    {
        for (auto v : p->val_param_spec_->vars_)
        {
            constants_[parent_name_].insert_utf8(v.first);
        }
    }

    proc_decl->body_->accept(*this);

    parent_name_ = temp;
}

void pascal2java::TableFillerVisitor::visit(
    ValueParameterSpecification* val_par_spec)
{
}

void pascal2java::TableFillerVisitor::visit(
    VariableParameterSpecification* var_par_spec)
{
}

void pascal2java::TableFillerVisitor::visit(FunctionHeading* func_heading)
{
    int name_number = constants_[parent_name_]
        .insert_utf8(func_heading->name_->name_);

    QString type = "(";

    for (FormalParameterSection* param : func_heading->params_)
    {
        for (const auto& pair : param->val_param_spec_->vars_)
        {
            if (TypeId* type_id = dynamic_cast<TypeId*>(pair.second))
            {
                if (type_id->type_ == "integer")
                {
                    type += "I";
                }
                else if (type_id->type_ == "char")
                {
                    type += "Ljava/lang/String;";
                }
            }
            else if (
                TypeArray* type_array = dynamic_cast<TypeArray*>(pair.second))
            {
                TypeId* type_id =
                    dynamic_cast<TypeId*>(type_array->type_denoter_);

                if (type_id->type_ == "integer")
                {
                    type += "[I";
                }
                else if (type_id->type_ == "char")
                {
                    type += "[Ljava/lang/String;";
                }
            }
        }
    }

    type += ")";

    if (TypeId* type_id = dynamic_cast<TypeId*>(func_heading->return_type_))
    {
        if (type_id->type_ == "integer")
        {
            type += "I";
        }
        else if (type_id->type_ == "char")
        {
            type += "Ljava/lang/String;";
        }
    }
    else if (TypeArray* type_array =
        dynamic_cast<TypeArray*>(func_heading->return_type_))
    {
        TypeId* type_id =
            dynamic_cast<TypeId*>(type_array->type_denoter_);

        if (type_id->type_ == "integer")
        {
            type += "[I";
        }
        else if (type_id->type_ == "char")
        {
            type += "[Ljava/lang/String;";
        }
    }

    int type_number = constants_[parent_name_].insert_or_find_utf8(type);

    int name_and_type_number = constants_[parent_name_].insert_name_and_type(
        name_number, type_number
    );

    int class_number = constants_[parent_name_].find_class(parent_name_);
    constants_[parent_name_]
        .insert_method_ref(class_number, name_and_type_number);
}

void pascal2java::TableFillerVisitor::visit(
    FormalParameterSection* formal_param_sec)
{
}

void pascal2java::TableFillerVisitor::visit(ProcedureIdentification* proc_ident)
{
}

void pascal2java::TableFillerVisitor::visit(FunctionIdentification* func_ident)
{
}

void pascal2java::TableFillerVisitor::visit(ProcedureHeading* proc_heading)
{
    int name_number = constants_[parent_name_]
        .insert_utf8(proc_heading->name_->name_);

    QString type = "(";

    for (FormalParameterSection* param : proc_heading->params_)
    {
        for (const auto& pair : param->val_param_spec_->vars_)
        {
            if (TypeId* type_id = dynamic_cast<TypeId*>(pair.second))
            {
                if (type_id->type_ == "integer")
                {
                    type += "I";
                }
                else if (type_id->type_ == "char")
                {
                    type += "Ljava/lang/String;";
                }
            }
            else if (
                TypeArray* type_array = dynamic_cast<TypeArray*>(pair.second))
            {
                TypeId* type_id =
                    dynamic_cast<TypeId*>(type_array->type_denoter_);

                if (type_id->type_ == "integer")
                {
                    type += "[I";
                }
                else if (type_id->type_ == "char")
                {
                    type += "[Ljava/lang/String;";
                }
            }
        }
    }

    type += ")V";
    int type_number = constants_[parent_name_].insert_or_find_utf8(type);

    int name_and_type_number = constants_[parent_name_].insert_name_and_type(
        name_number, type_number
    );

    int class_number = constants_[parent_name_].find_class(parent_name_);
    constants_[parent_name_]
        .insert_method_ref(class_number, name_and_type_number);
}

void pascal2java::TableFillerVisitor::visit(
    ProcOrFuncDeclaration* proc_or_func_decl)
{
    if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::PROCEDURE)
    {
        proc_or_func_decl->proc_decl_->accept(*this);
    }
    else if (proc_or_func_decl->type_ == ProcOrFuncDeclaration::Type::FUNCTION)
    {
        proc_or_func_decl->func_decl_->accept(*this);
    }
}

void pascal2java::TableFillerVisitor::visit(ActualParameter* actual_parameter)
{
    actual_parameter->expr_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(FunctionDesignator* func_designator)
{
    for (auto p : func_designator->params_)
    {
        p->accept(*this);
    }
}

void pascal2java::TableFillerVisitor::visit(VariableIdAccess* var_id_access)
{
}

void pascal2java::TableFillerVisitor::visit(IndexedVariable* indexed_variable)
{
}

void pascal2java::TableFillerVisitor::visit(
    WriteToIndexedVariable* indexed_variable)
{
}

void pascal2java::TableFillerVisitor::visit(AssignmentStatement* statement)
{
    statement->expr_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(ProcedureStatement* statement)
{
    for(auto p : statement->params_)
    {
        p->accept(*this);
    }
}

void pascal2java::TableFillerVisitor::visit(CompoundStatement* statement)
{
    for (Statement* stmt : statement->stmts_)
    {
        stmt->accept(*this);
    }
}

void pascal2java::TableFillerVisitor::visit(CaseStatement* statement)
{
    statement->expr_->accept(*this);

    for (auto e : statement->elems_)
    {
        e->accept(*this);
    }
}

void pascal2java::TableFillerVisitor::visit(CaseListElement* case_list_elem)
{
    case_list_elem->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(RepeatStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(IfWithoutElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(IfElseStatement* statement)
{
    statement->condition_->accept(*this);
    statement->stmt_->accept(*this);
    statement->else_stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(WhileStatement* statement)
{
    statement->expr_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(ForToStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(ForDowntoStatement* statement)
{
    statement->from_->accept(*this);
    statement->to_->accept(*this);
    statement->stmt_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(EqualExpr* equal_expr)
{
    equal_expr->left_->accept(*this);
    equal_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(NotEqualExpr* not_equal_expr)
{
    not_equal_expr->left_->accept(*this);
    not_equal_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(LtExpr* lt_expr)
{
    lt_expr->left_->accept(*this);
    lt_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(GtExpr* gt_expr)
{
    gt_expr->left_->accept(*this);
    gt_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(LeExpr* le_expr)
{
    le_expr->left_->accept(*this);
    le_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(GeExpr* ge_expr)
{
    ge_expr->left_->accept(*this);
    ge_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(AndExpr* and_expr)
{
    and_expr->left_->accept(*this);
    and_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(OrExpr* or_expr)
{
    or_expr->left_->accept(*this);
    or_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(NotExpr* not_expr)
{
    not_expr->expr_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(PlusExpr* plus_expr)
{
    plus_expr->left_->accept(*this);
    plus_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(MinusExpr* minus_expr)
{
    minus_expr->left_->accept(*this);
    minus_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(StarExpr* star_expr)
{
    star_expr->left_->accept(*this);
    star_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(SlashExpr* slash_expr)
{
    slash_expr->left_->accept(*this);
    slash_expr->right_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(DivExpr* div_expr)
{
}

void pascal2java::TableFillerVisitor::visit(ModExpr* mod_expr)
{
}

void pascal2java::TableFillerVisitor::visit(UnaryMinus* minus)
{
    minus->expr_->accept(*this);
}

void pascal2java::TableFillerVisitor::visit(Constant* constant)
{
    if (is_constant_)
    {
        int number = -1;

        switch (constant->const_type())
        {
            case Constant::Type::STRING:
                number = constants_[parent_name_]
                    .insert_or_find_utf8("Ljava/lang/String;");
                break;
            case Constant::Type::NUMBER:
                number = constants_[parent_name_]
                    .insert_or_find_utf8("I");
                break;
            case Constant::Type::ID:
            {
                int utf8_number = constants_[parent_name_].find_utf8(
                    constant->get_string_or_id()
                );
                number = constants_[parent_name_].find_type(utf8_number);
                break;
            }
        }

        int n = constants_[parent_name_]
            .insert_name_and_type(constant_number_, number);
        int class_number = constants_[parent_name_].find_class(parent_name_);
        constants_[parent_name_].insert_field_ref(class_number, n);
    }
    else
    {
        switch (constant->const_type())
        {
            case Constant::Type::STRING:
                int n = constants_[program_name_]
                    .insert_or_find_utf8(constant->get_string_or_id());
                constants_[program_name_].insert_string(n);
                break;
        }
    }
}

bool pascal2java::TableFillerVisitor::is_global() const
{
    return program_name_ == parent_name_;
}
