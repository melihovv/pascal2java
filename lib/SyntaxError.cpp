#include <SyntaxError.hpp>
#include <sstream>

pascal2java::SyntaxError::SyntaxError(
    const pascal2java::location& location,
    const QString& message)
{
    location_ = location;
    message_ = message;
}

QString pascal2java::SyntaxError::error() const
{
    std::ostringstream stream;
    stream << location_;
    QString result = QString::fromStdString(stream.str());
    return result + ": " + message_;
}
