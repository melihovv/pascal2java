#include <vld.h>
#include <fstream>
#include <iostream>
#include "../lib/Driver.hpp"
#include "../lib/TreeTransformerVisitor.hpp"
#include "../lib/TableFillerVisitor.hpp"
#include "../lib/DotGeneratorVisitor.hpp"
#include "../lib/DeletingVisitor.hpp"
#include "../lib/DotWriter.hpp"
#include "../lib/ConstantTablesWriter.hpp"
#include "../lib/TreeAttributingVisitor.hpp"
#include "../lib/SemanticErrorsCheckerWithoutTypeInfoVisitor.hpp"
#include "../lib/SemanticErrorsCheckerWithTypeInfoVisitor.hpp"
#include "../lib/BytecodeGeneratorVisitor.hpp"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Invalid argument count" << std::endl;
        std::cerr << "Usage: pascal2java.exe program.pas" << std::endl;
        return 1;
    }

    std::ifstream file{argv[1]};
    pascal2java::Driver d{&file};
    d.parse();

    if (d.errors().length() != 0)
    {
        return -1;
    }

    pascal2java::Program* p = d.root();

    try
    {
        pascal2java::TreeTransformerVisitor tree_transformer_visitor;
        p->accept(tree_transformer_visitor);

        pascal2java::SemanticErrorsCheckerWithoutTypeInfoVisitor
            semantic_errors_checker_without_type_info_visitor;
        p->accept(semantic_errors_checker_without_type_info_visitor);

        pascal2java::TableFillerVisitor table_filler_visitor{d.constants()};
        p->accept(table_filler_visitor);

        pascal2java::TreeAttributingVisitor tree_attributing_visitor;
        p->accept(tree_attributing_visitor);

//        pascal2java::SemanticErrorsCheckerWithTypeInfoVisitor
//            semantic_errors_checker_with_type_info_visitor;
//        p->accept(semantic_errors_checker_with_type_info_visitor);

        pascal2java::BytecodeGeneratorVisitor bytecode_generator(d.constants());
        p->accept(bytecode_generator);

        pascal2java::ConstantTablesWriter::write(
            "resources/tables.txt",
            d.constants(),
            p->name_);

        pascal2java::DotGeneratorVisitor dot_generator_visitor;
        p->accept(dot_generator_visitor);

        QString dot = dot_generator_visitor.dot();
        pascal2java::DotWriter::write("resources/dot/program.dot", dot);

        pascal2java::DeletingVisitor del_visitor;
        p->accept(del_visitor);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;

        pascal2java::DeletingVisitor del_visitor;
        p->accept(del_visitor);
    }

    return 0;
}
